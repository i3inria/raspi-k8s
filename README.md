# Container Orchestration and Edge Computing

This repository is primarily about **automating the installation of Kubernetes** on a cluster of Raspberry Pi 
(Raspbian) using **Ansible**, and experiment with **Edge Computing solutions**.  

<p align="center">
    <img src="/documentation/images/raspberry.png"  height="60">
    <img src="/documentation/images/ansible.png"  height="60">
    <img src="/documentation/images/docker.png"  height="60">
    <img src="/documentation/images/kubernetes.png"  height="60">
</p>

## Disclaimer

**Note**: if you spot a mistake or get an error while using the resources in this repository, do not hesitate 
to **open an issue, or best, make a PR**.

The code and documentation in this repository is valid at the time of writing. Docker, Kubernetes and the serverless 
frameworks evolve quickly and some configurations may not be relevant in a few months. Furthermore, K8S integrates 
many external components (like weave of flannel for networking pods) have their own roadmap and may introduce their 
own conflicts. 


## Repository structure

The **[documentation](https://gitlab.inria.fr/i3inria/raspi-k8s/tree/master/documentation)** section provides an overview
of Kubernetes related technologies as well as Edge Computing projetcs, and a detailed descriotion of the k8s installation.

The **[cluster](https://gitlab.inria.fr/i3inria/raspi-k8s/tree/master/cluster)** section provides all the setup scripts to 
setup a cluster of RPi and deploy Kubernetes on it using Ansible.

The **[rpi]()** section groups various sub-projects aimed at building Docker images suitable for the RPi cluster and Edge 
computing experiments (e.g., MQTT servers, Node-RED, Tensor-flow based object detection...).

The **[tutorial]()** section contains basic user guides and sample files on the use of Ansible and Kubernetes.

<p align="center">
    <img src="/documentation/images/rpi_clusters.png" height="240">
</p>



## Where to start?

- In the documentation folder, you'll find many documents to set up a cluster of RPi (i.e., basic OS and networking setup), install Docker and Kubernetes on such cluster, as well as buiding various pods (applications in Kubernetes) or using ansible
- In the cluster folder, you'll find all the Ansible scripts to automate the system, docker, and kubernetes installation on a cluster of RPi.
- In the rpi folder, you'll find various projects to build RPi Docker images for using sensors, deploying a mqtt registry, object detection, ... 

## Release notes

### Install V1: k8s 1.10 and kubeadm

The tagged V1.1 version of the repository dates back from 2019 and uses kubeadm to setup the kubernetes 
cluster (v1.10 as limitations for k8s dashboard on arm while 1.11 was available at that time on Raspebrry PI).
Installation was complex and error prone.

Ansible playbooks are used for the install (Ansible v2.5 - 2.6) and Docker v18.04 (version dependency for Kubernetes).

### Install V2: k8s 1.18 and k3s

The new install procedure, as of august 2021, uses k3s, an IoT focused k8s distribution for IoT devices built by Rancher. 
This install uses Docker 20.10 and Kubernetes 1.18. Also, this version updates to the Ansible playbooks (now Ansible v4). 

Arkade is then used to install applications in Kubernetes (e.g., Dashboard).

## References

This project builds on many other projects about k8s/Raspian, most notably:
- Alex Ellis [K8S on Raspian](https://github.com/teamserverless/k8s-on-raspbian) 
project.


*Pierre-Guillaume Raverdy (pierre-guillaume.raverdy@inria.fr)*
