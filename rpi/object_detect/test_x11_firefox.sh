#!/bin/bash

# use this script to test forwarding X11 window in container on a Mac OSX host with xquarts
# uses special DISPLAY name

# allow access from localhost
xhost + 127.0.0.1

# run firefox with X11 forwarding and keep running until it quits
docker run -e DISPLAY=host.docker.internal:0 jess/firefox
