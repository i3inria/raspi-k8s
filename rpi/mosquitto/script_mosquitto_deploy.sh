#!/bin/bash

# Script to create the default mosquito user and group locallyy,
# the volume to make data persistent, and then deploy the
# mosquitto docker container from the local registry

# itd : interactive (so disconnect ssh, no output from container), detached mode
# restart=always : try to restart the container if it crashes, or strart on boot
# ports redirection: 1883 and 9001
# (not used) --net=host : gives full network access to container, allow accessing it thru 127.0.0.1



addgroup --gid 10100 sharedvol

mkdir -p /data/
mkdir -p /data/mosquitto/

chown :10100 /data/mosquitto
chmod 775 /data/mosquitto
chmod g+s /data/mosquitto

mkdir -p /data/mosquitto/conf/
mkdir -p /data/mosquitto/data/
mkdir -p /data/mosquitto/logs/

docker run \
    -itd \
    -p 1883:1883 \
    -p 9001:9001 \
    --restart=always \
    --name=mosquito \
    -v /data/mosquitto/conf:/mosquitto/conf:ro \
    -v /data/mosquitto/log:/mosquitto/log \
    -v /data/mosquitto/data/:/mosquitto/data/ \
    pimaster.local:443/mosquitto:default

# volume based
#    -v /volume/docker/mosquitto/conf:/mosquitto/conf:ro \
#    -v /volume/docker/mosquitto/log:/mosquitto/log \
#    -v /volume/docker/mosquitto/data/:/mosquitto/data/ \
