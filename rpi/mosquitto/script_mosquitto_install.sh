#!/bin/bash

# Script to build and push the docker mosquitto image
# and configure the local volumes with logs, data and security
#

docker build -t mosquitto:default .
docker tag mosquitto:default pimaster.local:443/mosquitto:default
docker push pimaster.local:443/mosquitto:default

