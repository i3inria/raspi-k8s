# Raspberry Pi Projects

This folder is dedicated to the creation of Docker images on Raspberry Pi / Debian Stretch for the different components
potentially involved in the deployment of a crowdsourcing platform such as MQTT message brokers, databases, workflow
managers ...

## Projects
- **Mosquitto** is an MQTT server. 
- **Node-RED** is to deploy the Node-RED server on a RPi and access its GPIO. 
- **object_detect** is an application based on tesnor-flow to do object detection using the i2c camera.. 
- **sensehat** is a set of application to use/access the SenseHat board in Python. 


## FAQ

### Get list of images in Docker registry

curl -X GET https://myregistry:5000/v2/_catalog

curl -X GET https://myregistry:5000/v2/ubuntu/tags/list

### Cluster monitoring

Install ELK Docker compose on the monitoring machine Follow [instructions](https://elk-docker.readthedocs.io/) 
and [here](https://hub.docker.com/r/sebp/elk/)


### Answer yes to an install bash script

    yes | bash <(curls ....url absh script)...)

### Locale

Cannot setlocale: LC_ALL: Check [issue/solution](https://raspberrypi.stackexchange.com/questions/43550/unable-to-reconfigure-locale-in-raspberry-pi)

    Edit /etc/locale.gen and uncomment the line with en_US.UTF-8 e.g. sudo nano /etc/locale.gen
    uncomment line by deleting leading #

    Run sudo locale-gen en_US.UTF-8
    Run sudo update-locale en_US.UTF-8

### Identify which process uses a givent port

    netstat -lntup

Then can use pid to get the process. If the PID column is empty, may need to run as root.
In some cases, ports are used by NFS mounts,, check this with `rpcinfo -p`.

Check addtional info/specific cases [here](https://serverfault.com/questions/311009/netstat-shows-a-listening-port-with-no-pid-but-lsof-does-not) 
or [here](https://www.cyberciti.biz/faq/unix-linux-check-if-port-is-in-use-command/).


### Priority routing between eth0and wlan0

If both network interfaces are up, DNS and gateway routing should be configured properly otherwise messages may not 
be routed properly (cannot resolve names, cannot route packet to destination)

Multiple issues here. Related to setting up properly the DNS servers for each interface, and also setting up the proper 
route.

Issue with the `default` route for the packets using the `eth0` interface.

If, for example, the wlan0 interface is the one to be used while eth0 will only be used for link local interactions 
(e.g., wlan0 is for app/internet use, and eth0 is only to connect to a monitoring server), the a line with `nogateway` 
can be added to the static interface definition in `/etc/dhcpcd.conf`

Check the [discussion](https://unix.stackexchange.com/questions/182967/can-i-prevent-a-default-route-being-added-when-bringing-up-an-interface)

In the pinode1 node

    # BEGIN ANSIBLE MANAGED BLOCK 
    interface eth0
    static ip_address=10.0.0.61/24
    static ip6_address=fd51:42f8:caae:82::feef/64
    static routers=10.0.0.1
    static domain_name_servers=10.0.0.1 192.168.0.254 8.8.8.8
    nogateway
    interface wlan0
    static ip_address=192.168.0.61/24
    static ip6_address=2a01:e35:39f0:11c0:564d:9ac7:864d:d22f/64
    static routers=192.168.0.254
    static domain_name_servers=192.168.0.254 10.0.0.1 8.8.8.8
    # END ANSIBLE MANAGED BLOCK 

Then routing info, `default` (0.0.0.0) route will use `wlan0` as `eth0` has `nogateway` flag.

    pi@pinode1:~ $ route
    Kernel IP routing table
    Destination     Gateway         Genmask         Flags Metric Ref    Use Iface
    default         192.168.0.254   0.0.0.0         UG    303    0        0 wlan0
    10.0.0.0        0.0.0.0         255.255.255.0   U     202    0        0 eth0
    172.17.0.0      0.0.0.0         255.255.0.0     U     0      0        0 docker0
    192.168.0.0     0.0.0.0         255.255.255.0   U     303    0        0 wlan0

For routing, can also use the command `netstat -rn` or `ìp route`` Check [here](https://serverfault.com/questions/523388/what-is-the-difference-between-route-and-ip-route)
for a discussion on net-tools and its replacement iproute2, and the various network commands.


Alternatively  [here](https://raspberrypi.stackexchange.com/questions/40228/make-permanent-change-in-the-routing-table), 
if both interfaces may be used for internet routing, but a priority must be specified, can 

Use commands to reorder the routing

    sudo route delete  default gateway 192.168.137.1 

and then :

    sudo route add  default gateway 192.168.137.1

Wil make sure the route deleted/added is the first one to be used

Or can use the metric solution exposed (lower the metric, higher the priority, default is 200 but 300 for wireless)

In `/etc/dhcpcd.conf` add

    interface wlan0;
    metric 200;
    
    interface eth0
    metric 300;


**Ubuntu specific**: metric can be changed with the following commands

    sudo ifmetric wlan0 50

Additional info/explanations [here](https://superuser.com/questions/331720/how-do-i-set-the-priority-of-network-connections-in-ubuntu)


