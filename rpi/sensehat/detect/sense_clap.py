#! /usr/bin/python3
from sense_hat import SenseHat
from time import sleep
import csv
import random
import time

import pandas as pd
import matplotlib.pyplot as plt
import math
import numpy as np
import scipy
from scipy.signal import find_peaks

sense = SenseHat()

fieldnames = ['ts', 'date', 'x', 'y', 'z', 'pitch', 'roll', 'yaw', 'mx','my','mz']

data = []

window_size = 10

# app main function
def entry_func():
    sense = SenseHat()

    all_windows_peaks = []

    sense.clear((0, 0, 0))

    while True:
        acceleration = sense.get_accelerometer_raw()
        orientation = sense.get_orientation()
        compas = sense.get_compass_raw()

        timestamp = time.time()
        now = time.strftime('%d-%m-%Y %H:%M:%S')

        x = acceleration['x']
        y = acceleration['y']
        z = acceleration['z']

        mx = compas['x']
        my = compas['y']
        mz = compas['z']

        pitch = orientation["pitch"]
        roll = orientation["roll"]
        yaw = orientation["yaw"]

        data.append([timestamp,now,x,y,z,pitch,roll,yaw,mx,my,mz])

        if (len(data) > window_size):
            indice = 1
            start = len(data)-window_size-1
            end = len(data)-1
            df = pd.DataFrame(data[start:end:indice], columns=fieldnames)

            # compute the mean for each accelerometer values
            mean_x = df["x"].mean()
            mean_y = df["y"].mean()
            mean_z = df["z"].mean()
            # and generate new values for accelerometer centered around the mean
            df["center_x"] = df.x - mean_x
            df["center_y"] = df.y - mean_y
            df["center_z"] = df.z - mean_z
            # and again squareroot ....
            df["accel_mean_sqrt"] = np.sqrt(df["center_x"] ** 2 + df["center_y"] ** 2 + df["center_z"] ** 2)

            peaks_mean_accel, _ = find_peaks(df.accel_mean_sqrt, prominence=1, distance=20)

            for item in peaks_mean_accel:
                value = start + item
                if not value in all_windows_peaks:
                    # print("Peak not found {0}".format(value))
                    all_windows_peaks.append(value)

                    sense.clear((randint(0,255), randint(0,255), randint(0,255)))

            print (all_windows_peaks)

            delay = time.time() - timestamp
            print ("delay {:.3f}".format(delay))



#
#  entry point to launch entry_func by default
#
if __name__ == '__main__':
    entry_func()
