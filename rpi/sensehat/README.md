# Raspeberry Sense HAT

This folder provides a Docker image with various apps interacting with the different sensors/actuators of the Raspberry 
SenseHat thru GPIO. 

<img src="assets/raspberry-pi-sense-hati.jpg" width="200" alt="">

The SenseHat [hardware info](https://www.raspberrypi.org/documentation/hardware/sense-hat/)

The apps are based on the following [tutorial](https://projects.raspberrypi.org/en/projects/getting-started-with-the-sense-hat)

The Python SenseHAT library [documentation](https://pythonhosted.org/sense-hat/).

## Apps overview

**main.py**  : a wrapper app that display the names of the available apps. User can scroll thru the apps with the 
joystcik and launch the app by clicking it.

- Using the LED matrix (color, display images)
    - display_color.py
    - display_image.py
    - display_image2.py
- Using the LED matrix to display/scroll text.
    - display_rotate_text.py
    - display_text.py
    - show_letter.py
    - show_letters.py
    - show_random_letters.py
- Animation with the LED matrix
    - flip_image.py
- Using the joystick
    - joy_led.py
    - joy_led2.py
    - joystick.py
- Using the IMU sensor
    - sense_acceleration.py
    - sense_all.py
    - sense_orientation.py
    - shake.py
- Using the environment sensor
    - sense_humid.py
    - sense_pressure.py
    - sense_temp.py
- General
    - challenge.py
 
### Installation

## Docker

### Dockerfile

### Launching the container

Need to mount the relevant devices to access the GPIO ports. Either use the `--privileged` flag or specify each required device.

For the current image, the devices are /dev/fb0 /dev/fb1 /dev/i2c-1 /dev/input/event0.


## Raspberry Pi 4

Issue with headless booting on RPi4 with Sense Hat and [solution](https://forums.balena.io/t/raspberry-pi-4-sense-hat-support-on-balenaos-64bit-beta/32254)

## Pi Zero WH SenseHat B

A few notes on the Pi Zero WH
- its quite slow compared to Pi3 (only 1 core)
- WiFi is very sensitive to interferences, try to use channel 1 or 12, remove nearby WiFi devices
- its armv6 and not armv7 (`dmesg`), so many packages are not available (in particular miniconda)

<img src="assets/zerp\ sensehat.png" width="200" alt="">

A SenseHat for the PiZero is available [here](https://www.amazon.fr/IBest-Sense-Raspberry-Accelerometer-Magnetometer/dp/B08H5JH51Y/ref=sr_1_1?__mk_fr_FR=%C3%85M%C3%85%C5%BD%C3%95%C3%91&keywords=sense+hat+pi+zero&qid=1635957059&qsid=260-9319209-7233554&s=computers&sr=1-1&sres=B08H5JH51Y%2CB07PPV122Z%2CB07PMX48K4%2CB014T2IHQ8%2CB08GPGDWKC%2CB085LQT67P%2CB07BHMRTTY%2CB06WD5FTVD%2CB014PF05ZA%2CB07WQT1RRZ%2CB06X1BPH8D%2CB08C254F73%2CB07Q7W26N8%2CB07VNRHL6T%2CB075FQKSZ9%2CB08XJJ7R37&srpt=PERSONAL_COMPUTER#productDetails)
Manufacturer info is available [here](https://www.waveshare.com/sense-hat-b.htm) with specific [dev resources](https://www.waveshare.com/wiki/Sense_HAT_(B))

### Setup

To make the SenseHat work/recognized, need to do **rpi update**
```
sudo rpi-update
sudo reboot
```

Then the configuration and system package install
```
sudo apt update
sudo apt upgrade
```

Enable I2C
```
raspi-config
sudo reboot
```

Python 3 install with pip if not done before 
```
sudo apt-get install python3-pip
```

Install unzip for 7z then useage is `7z x filename.7z`
```
sudo apt-get install p7zip-full
```

Install smbus and other packages
```
sudo apt install libtiff5-dev
sudo apt install python-smbus
sudo apt install libatlas-base-dev libopenjp2-7 libtiff-tools sense-hat
sudo apt-get install libatlas-base-dev
sudo apt-get install libopenjp2-7 sense-hat
pip install sense-hat rtimulib
pip3 install smbus
sudo apt-get install python-rpi.gpio
```

Install wiringpi
- visit [http://wiringpi.com/](http://wiringpi.com/)





















# install numpy via apt ?
pip3 uninstall numpy
apt install python3-numpy

## References

Launch Docker container
- [link](https://stackoverflow.com/questions/30059784/docker-access-to-raspberry-pi-gpio-pins)

RPi, Docker and i2c
- [link](https://github.com/knomic/raspian-i2c-base-image/blob/master/Dockerfile)
- [link](https://www.the-captains-shack.com/post/rpi-piglow-container/)
https://stackoverflow.com/questions/40265984/i2c-inside-a-docker-container

RPi sensing hats
- [FR link](https://raspberry-pi.developpez.com/actu/259981/Le-TOP-10-des-cartes-d-extension-HAT-pour-Raspberry-Pi-votez-pour-vos-cartes-Pi-HAT-preferees/)

[About i2c and other interfaces] (https://www.balena.io/docs/learn/develop/hardware/i2c-and-spi/)

Docker and GPIO
- [link](https://stackoverflow.com/questions/56367707/docker-image-cant-see-gpio-pins-with-privileged-set-to-true-within-iot-edge-con)
- [link](https://stackoverflow.com/questions/53094979/how-to-enable-wiringpi-gpio-control-inside-a-docker-container)
- [link](https://stackoverflow.com/questions/57963558/making-raspberry-pi-gpio-work-from-an-azure-iot-edge-module
)

