import pandas as pd
import matplotlib.pyplot as plt
import math
import numpy as np
import time
import scipy
from scipy.signal import find_peaks

print(time.time())

# read the csv file, condider a header.
df = pd.read_csv('IMU_data_wrist_right.csv', sep=',', header=0)
#df = pd.read_csv('IMU_data.csv', sep=',', header=0)

subplots_ts_old = np.array([[0.0, 65.0, "Quiet 1"],
                        [55.0, 125.0, "Clap 1"],
                        [115.0, 185.0, "Quiet 2"],
                        [175, 245, "Move 1"],
                        [235, 305, "Quiet 3"],
                        [295, 365, "Clap/move 2"],
                        [355, 425, "Clap/move 3"],
                        [415, 485, "Quiet 4"],
                        [475, 545, "Move 2"],
                        [535, 605, "Quiet 5"],
                        [595, 665], "Move and heel"]
                           )

subplots_ts = np.array([
    [0.0, 65.0], #, "nothing"
    [60.0, 130.0], #, "clap light"
    [120.0, 190.0], #, "clap heavy"
    [180.0, 250.0], #, "nothin"
    [240.0, 310.0], #, "double clap"
    [300.0, 370.0], #, "nothing"
    [360.0, 430.0], #, "move"
    [420.0, 490.0], #, "move and clap"
    [480.0, 550.0], #, "free move"
    [540.0, 610.0] #, "???"
  ])

# removes the initial ts to start at zero (int)
init_ts = df.loc[1, 'ts']
df.ts = df.ts - int(init_ts)

print("removed initial ts of {0} ts1 {1} ts2 {2} entries".format(init_ts, df.loc[1, 'ts'], df.loc[2, 'ts']))

# compute squareroot of sum of square for accelerometer
df["accel_sqrt"] = np.sqrt(df["x"] ** 2 + df["y"] ** 2 + df["z"] ** 2)

# compute an average with neighbors for all accelerometer values
for i in range(2, len(df) - 1):
    df.loc[i, 'loc_avg_x'] = (df.loc[i - 1, 'x'] * df.loc[i, 'x'] + df.loc[i + 1, 'x']) / 3
    df.loc[i, 'loc_avg_y'] = (df.loc[i - 1, 'y'] * df.loc[i, 'y'] + df.loc[i + 1, 'y']) / 3
    df.loc[i, 'loc_avg_z'] = (df.loc[i - 1, 'z'] * df.loc[i, 'z'] + df.loc[i + 1, 'z']) / 3
# compute square root of sum of square for accelerometer averaged values
df["loc_avg_accel_sqrt"] = np.sqrt(df["loc_avg_x"] ** 2 + df["loc_avg_y"] ** 2 + df["loc_avg_z"] ** 2)

# compute the mean for each accelerometer values
mean_x = df["x"].mean()
mean_y = df["y"].mean()
mean_z = df["z"].mean()
# and generate new values for accelerometer centered around the mean
df["center_x"] = df.x - mean_x
df["center_y"] = df.y - mean_y
df["center_z"] = df.z - mean_z
# and again squareroot ....
df["accel_mean_sqrt"] = np.sqrt(df["center_x"] ** 2 + df["center_y"] ** 2 + df["center_z"] ** 2)


#
# plotting
#

# ax.plot(trig, y, ls="", marker="*", ms=15,  color="crimson")
# numpy.interp

if True:
    period = 4
    subplot_ts = subplots_ts[period]
    subplot_df = df[(df['ts'] > subplot_ts[0] * 1) & (df['ts'] < subplot_ts[1] * 1)]

    timestamps = subplot_df.ts

    # find peaks based on prominence and widht on mean sqrt accel - full view
    peaks_mean_accel, _  = find_peaks(subplot_df.accel_mean_sqrt, prominence=1, distance=20)

    print (peaks_mean_accel)

    print("subplot size {0} entries".format(len(subplot_df)))



    all_windows_peaks_50_0  = []
    for index_window in range(1, len(subplot_df) - 50):
        window_df = subplot_df[index_window:index_window+50]
        window_peaks, _  = find_peaks(window_df.accel_mean_sqrt, prominence=1, distance=20)

        for item in window_peaks:
            value = index_window + item
            if not value in all_windows_peaks_50_0:
                # print("Peak not found {0}".format(value))
                all_windows_peaks_50_0.append(value)
    print (all_windows_peaks_50_0)

    all_windows_peaks_20_0  = []
    for index_window in range(1, len(subplot_df) - 20):
        window_df = subplot_df[index_window:index_window+20]
        window_peaks, _  = find_peaks(window_df.accel_mean_sqrt, prominence=1, distance=20)

        for item in window_peaks:
            value = index_window + item
            if not value in all_windows_peaks_20_0:
                # print("Peak not found {0}".format(value))
                all_windows_peaks_20_0.append(value)

    print (all_windows_peaks_20_0)

    all_windows_peaks_10_0  = []
    for index_window in range(1, len(subplot_df) - 10):
        window_df = subplot_df[index_window:index_window+10]
        window_peaks, _  = find_peaks(window_df.accel_mean_sqrt, prominence=1, distance=20)

        for item in window_peaks:
            value = index_window + item
            if not value in all_windows_peaks_10_0:
                # print("Peak not found {0}".format(value))
                all_windows_peaks_10_0.append(value)

    print (all_windows_peaks_10_0)

    all_windows_peaks_50_10  = []
    for index_window in range(1, len(subplot_df) - 50):
        window_df = subplot_df[index_window:index_window+50]
        window_peaks, _  = find_peaks(window_df.accel_mean_sqrt, prominence=1, distance=20)

        for item in window_peaks:
            if item <= (50-10):
                value = index_window + item
                if not value in all_windows_peaks_50_10:
                    # print("Peak not found {0}".format(value))
                    all_windows_peaks_50_10.append(value)

    print (all_windows_peaks_50_10)

    all_windows_peaks_20_10  = []
    for index_window in range(1, len(subplot_df) - 20):
        window_df = subplot_df[index_window:index_window+20]
        window_peaks, _  = find_peaks(window_df.accel_mean_sqrt, prominence=1, distance=20)

        for item in window_peaks:
            if item <= (20-10):
                value = index_window + item
                if not value in all_windows_peaks_20_10:
                    # print("Peak not found {0}".format(value))
                    all_windows_peaks_20_10.append(value)

    print (all_windows_peaks_20_10)

    all_windows_peaks_20_5  = []
    for index_window in range(1, len(subplot_df) - 20):
        window_df = subplot_df[index_window:index_window+20]
        window_peaks, _  = find_peaks(window_df.accel_mean_sqrt, prominence=1, distance=20)

        for item in window_peaks:
            if item <= (20-5):
                value = index_window + item
                if not value in all_windows_peaks_20_5:
                    # print("Peak not found {0}".format(value))
                    all_windows_peaks_20_5.append(value)

    print (all_windows_peaks_20_5)

    all_windows_peaks_10_5  = []
    for index_window in range(1, len(subplot_df) - 10):
        window_df = subplot_df[index_window:index_window+10]
        window_peaks, _  = find_peaks(window_df.accel_mean_sqrt, prominence=1, distance=20)

        for item in window_peaks:
            if item <= (10-5):
                value = index_window + item
                if not value in all_windows_peaks_10_5:
                    # print("Peak not found {0}".format(value))
                    all_windows_peaks_10_5.append(value)

    print (all_windows_peaks_10_5)

    plt.subplot(4, 2, 1)
    plt.plot(timestamps, subplot_df.accel_mean_sqrt, label='Full view')
    plt.plot(subplot_df.iloc[peaks_mean_accel, :].ts, subplot_df.iloc[peaks_mean_accel, :].accel_mean_sqrt, "ob")

    plt.subplot(4, 2, 2)
    plt.plot(timestamps, subplot_df.accel_mean_sqrt, label='Window view')
    plt.plot(subplot_df.iloc[all_windows_peaks_50_0, :].ts, subplot_df.iloc[all_windows_peaks_50_0, :].accel_mean_sqrt, "ob")

    plt.subplot(4, 2, 3)
    plt.plot(timestamps, subplot_df.accel_mean_sqrt, label='Window view')
    plt.plot(subplot_df.iloc[all_windows_peaks_20_0, :].ts, subplot_df.iloc[all_windows_peaks_20_0, :].accel_mean_sqrt, "ob")

    plt.subplot(4, 2, 4)
    plt.plot(timestamps, subplot_df.accel_mean_sqrt, label='Window view')
    plt.plot(subplot_df.iloc[all_windows_peaks_10_0, :].ts, subplot_df.iloc[all_windows_peaks_10_0, :].accel_mean_sqrt, "ob")

    plt.subplot(4, 2, 5)
    plt.plot(timestamps, subplot_df.accel_mean_sqrt, label='Window view')
    plt.plot(subplot_df.iloc[all_windows_peaks_50_10, :].ts, subplot_df.iloc[all_windows_peaks_50_10, :].accel_mean_sqrt, "ob")

    plt.subplot(4, 2, 6)
    plt.plot(timestamps, subplot_df.accel_mean_sqrt, label='Window view')
    plt.plot(subplot_df.iloc[all_windows_peaks_20_10, :].ts, subplot_df.iloc[all_windows_peaks_20_10, :].accel_mean_sqrt, "ob")

    plt.subplot(4, 2, 7)
    plt.plot(timestamps, subplot_df.accel_mean_sqrt, label='Window view')
    plt.plot(subplot_df.iloc[all_windows_peaks_20_5, :].ts, subplot_df.iloc[all_windows_peaks_20_5, :].accel_mean_sqrt, "ob")

    plt.subplot(4, 2, 8)
    plt.plot(timestamps, subplot_df.accel_mean_sqrt, label='Window view')
    plt.plot(subplot_df.iloc[all_windows_peaks_10_5, :].ts, subplot_df.iloc[all_windows_peaks_10_5, :].accel_mean_sqrt, "ob")

    plt.show()


if False:
    # compute peaks from both sqrt and mean sqrt, using mainly prominence, and also distance
    period = 6
    subplot_ts = subplots_ts[period]
    subplot_df = df[(df['ts'] > subplot_ts[0] * 1) & (df['ts'] < subplot_ts[1] * 1)]

    timestamps = subplot_df.ts

    # find peaks based on prominence and widht
    peaks_accel, _  = find_peaks(subplot_df.accel_sqrt, prominence=2)
    peaks_mean_accel, _  = find_peaks(subplot_df.accel_mean_sqrt, prominence=1, distance=20)

    # print (subplot_df.iloc[peaks_accel, :].ts)

    plt.subplot(2, 1, 1)
    # ax.plot(timestamps, subplot_df.x, label='X accel')
    # ax.plot(timestamps, subplot_df.y, label='Y accel')
    # ax.plot(timestamps, subplot_df.z, label='Z accel')
    plt.plot(timestamps, subplot_df.accel_sqrt, label='Accel')
    plt.plot(subplot_df.iloc[peaks_accel, :].ts, subplot_df.iloc[peaks_accel, :].accel_sqrt, "ob")

    plt.subplot(2, 1, 2)
    plt.plot(timestamps, subplot_df.accel_mean_sqrt, label='Mean accel')
    plt.plot(subplot_df.iloc[peaks_mean_accel, :].ts, subplot_df.iloc[peaks_mean_accel, :].accel_mean_sqrt, "ob")

    # plt.legend(loc='best')
    plt.show()


if False:
    # for the first period, display and show markers on 3 specific places
    # display 2 lines -interpolate for the marker
    subplot_ts = subplots_ts[0]
    subplot_df = df[(df['ts'] > subplot_ts[0] * 1) & (df['ts'] < subplot_ts[1] * 1)]

    timestamps = subplot_df.ts

    fig, ax = plt.subplots()
    ax.plot(timestamps, subplot_df.accel_sqrt, label='Accel')
    ax.plot(timestamps, subplot_df.x, label='X')

    # ax.plot(subplot_df.iloc[peaks_accel, :].ts, subplot_df.iloc[peaks_accel, :].accel_sqrt, "ob")
    # ax.plot(peaks_mean_accel, subplot_df.accel_mean_sqrt[peaks_mean_accel], "vg")

    trig = np.array([5.4,30.3,39.1])
    # draw vert line at specific points
    for x in trig:
        ax.axvline(x, color="limegreen")
    # interpolate for x values
    y = np.interp(trig, timestamps, subplot_df.x)
    ax.plot(trig, y, ls="", marker="*", ms=15,  color="crimson")

    plt.legend(loc='best')
    plt.show()

if False:
    # grid of plots for X, Y, Z and Roll, Pitch, Yaw
    timestamps = df.ts

    plt.title('Gyro and accelerometer')

    plt.subplot(3, 2, 1)
    plt.plot(timestamps, df.x, '.-')
    plt.ylabel('X acceleration')

    plt.subplot(3, 2, 2)
    plt.plot(timestamps, df.y, '.-')
    plt.ylabel('Y acceleration')

    plt.subplot(3, 2, 3)
    plt.plot(timestamps, df.z, '.-')
    plt.ylabel('Z acceleration')

    plt.subplot(3, 2, 4)
    plt.plot(timestamps, df.roll, '.-')
    plt.ylabel('Roll')

    plt.subplot(3, 2, 5)
    plt.plot(timestamps, df.pitch, '.-')
    plt.ylabel('Pitch')

    plt.subplot(3, 2, 6)
    plt.plot(timestamps, df.yaw, '.-')
    plt.ylabel('Yaw')

    plt.show()

if False:
    # suvcessive plots of parts of the values

    for subplot_ts in subplots_ts:
        subplot_df = df[(df['ts'] > subplot_ts[0] * 1) & (df['ts'] < subplot_ts[1] * 1)]

        # subplot_df = df[(df.ts.isin([subplot_ts[0]*1000, subplot_ts[1]*1000] ))]
        print("subplot between {0} and {1} has {2} entries".format(subplot_ts[0], subplot_ts[1], len(subplot_df)))
        timestamps = subplot_df.ts

        plt.title('A tale of 2 subplots')

        plt.subplot(2, 1, 1)
        plt.plot(timestamps, subplot_df.accel_sqrt, '.-')
        plt.ylabel('accel_sqrt')
        plt.xlabel('time (s)')

        plt.subplot(2, 1, 2)
        plt.plot(timestamps, subplot_df.loc_avg_accel_sqrt, '.-')
        plt.plot(trig, y, ls="", marker="*", ms=15, color="crimson")
        plt.xlabel('time (s)')
        plt.ylabel('loc_avg_accel_sqrt')

        plt.show()

if False:
    # single fig with scatter plot of x accell
    timestamps = df.ts
    plt.scatter(timestamps, df.x)
    plt.show()  # or plt.savefig("name.png")



if False:
    # plot with 5 sub plots vertical with 1 value each
    timestamps = df.ts

    plt.title('A tale of 3 subplots')

    plt.subplot(5, 1, 1)
    plt.plot(timestamps, df.x, '.-')
    plt.ylabel('X acceleration')
    plt.xlabel('time (s)')

    plt.subplot(5, 1, 2)
    plt.plot(timestamps, df.y, '.-')
    plt.xlabel('time (s)')
    plt.ylabel('Y acceleration')

    plt.subplot(5, 1, 3)
    plt.plot(timestamps, df.z, '.-')
    plt.xlabel('time (s)')
    plt.ylabel('Z acceleration')

    plt.subplot(5, 1, 4)
    plt.plot(timestamps, df.loc_avg_x, '.-')
    plt.xlabel('time (s)')
    plt.ylabel('X loc_avg acceleration')

    plt.subplot(5, 1, 5)
    plt.plot(timestamps, df.accel_sqrt, '.-')
    plt.xlabel('time (s)')
    plt.ylabel('X/Y/Z sqrt')

    plt.savefig("output_full")
    plt.show()

# df.to_csv('output.csv', sep = ',')
