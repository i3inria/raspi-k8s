# -*- coding: latin-1 -*-
import smbus, time
bus = smbus.SMBus(1)

#ecriture du registre CTRL_REG1_G (0b11000011=0xc3)
bus.write_byte_data(0x6a, 0x10, 0x0c3)
print ("ecriture du registre CTRL_REG81_G 0x10: 0xc3 (ODR=952Hz, Cutoff=100Hz)")
#lectur(e du registre CTRL_REG1_G (pour voir si la modification � bien �t� prise en compte
ctrl_reg1 = bus.read_byte_data(0x6a, 0x10)
print ("lecture reg.  CTRL_REG1_G 0x10:0x%x"%ctrl_reg1) #affichage en hexa

#ecriture du registre CTRL_REG8 (0x04 passage du bit IF_ADD_INC � '1')
#ecriture du registre CTRL_REG8 (0x06 passage du bit IF_ADD_INC � '1' et Big/Little Endian � '1')
bus.write_byte_data(0x6a, 0x22, 0x04)
print ("ecriture du registre CTRL_REG8 0x04")
#lecture du registre CTRL_REG8 (pour voir si la modification � bien �t� prise en compte
ctrl_reg8 = bus.read_byte_data(0x6a, 0x22)
print ("lecture reg.  CTRL_REG8 0x22:0x%x"%ctrl_reg8) #affichage en hexa

while True:
    #lecture des registres du gyro axe z octet par mot de 16 bit
    data = bus.read_word_data(0x6a, 0x1c)

    #data_L = data >> 8
    #data_H = (data - data_L * 256)
    #data = data_H * 256 + data_L

    #la valeur est cod�e en compl�ment � 2
    if data >= 0x8000: data -= 65536
    #application de l'echelle de conversion pour avoir une mesure en dps
    valeur_Gz = data*245.0/(65536/2)
    #print ("lecture directe gyroscope Gz (mot de 16 bit):", "0x%x"%data_H, "0x%x"%data_L, valeur_Gz)
    print ("lecture directe gyroscope Gz (mot de 16 bit):", data, valeur_Gz)
    time.sleep(0.5)

