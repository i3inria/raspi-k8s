# -*- coding: latin-1 -*-
from sense_hat import SenseHat
import smbus
bus = smbus.SMBus(1)

#lecture et affichage en utilisant l'API officielle
#a noter que cela assure egalement la bonne initialisation du circuit
#il faudra donc le conserver m�me si c'est acc�s direct aux registres qui est utilis�
sense = SenseHat()
sense.set_imu_config(False, True, False)  # gyroscope only
raw = sense.get_gyroscope_raw()

print ("lecture avec l'API sens Hat:"),
print("x: {x}, y: {y}, z: {z}".format(**raw))

#verification d'un acces correct au circuit en lisant le registre who_i_am
who_i_am = bus.read_byte_data(0x6a, 0x0f) # adresse du circuit 0x6a, adresse du registre who_i_am 0x0f
print ("--LSM9DS1 read who_i_am : "),
print ("0x%x"%who_i_am) # affichage de la valeur lue en hexad�cimal

#lecture des registres du gyro axe z octet par octet
data_L = bus.read_byte_data(0x6a, 0x1c)
data_H = bus.read_byte_data(0x6a, 0x1d)
print ("lecture directe des registres : 0x1c:", data_L),
print (" - 0x1d:", data_H)

#reconstitution de mot de 16 bit � partir des deux octets
data = data_H*256 + data_L
#la valeur est cod�e en compl�ment � 2
if data >= 0x8000: data -= 65536
#application de l'echelle de conversion pour avoir une mesure en dps
valeur_Gz = data*245.0/(65536/2)
print ("lecture directe gyroscope Gz (octet par octet):", valeur_Gz)

#lecture du registre CTRL_REG8
ctrl_reg8 = bus.read_byte_data(0x6a, 0x22)
print ("lecture reg.  CTRL_REG8 0x22:0x%x"%ctrl_reg8) # affichage en hexa

#ecriture du registre CTRL_REG8 (passage du bit IF_ADD_INC � '1')
bus.write_byte_data(0x6a, 0x22, 0x04)
print ("ecriture du registre CTRL_REG8 0x04")

#re-lecture du registre CTRL_REG8 (pour voir si la modification � bien �t� prise en compte
ctrl_reg8 = bus.read_byte_data(0x6a, 0x22)
print ("lecture reg.  CTRL_REG8 0x22:0x%x"%ctrl_reg8) # affichage en hexa

#lecture des registres du gyro axe z octet par mot de 16 bit
data = bus.read_word_data(0x6a, 0x1c)
#reconstitution de mot de 16 bit � partir des deux octets
data_L = data >> 8
data_H = (data - data_L*256)
data = data_H*256 + data_L
#la valeur est cod�e en compl�ment � 2
if data >= 0x8000: data -= 65536
#application de l'echelle de conversion pour avoir une mesure en dps
valeur_Gz = data*245.0/(65536/2)
print ("lecture directe gyroscope Gz (mot de 16 bit):", valeur_Gz)