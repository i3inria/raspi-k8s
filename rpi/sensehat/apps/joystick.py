from sense_hat import SenseHat


# app main function
def entry_func():
    sense = SenseHat()

    while True:
        for event in sense.stick.get_events():
            print(event.direction, event.action)


#
#  entry point to launch entry_func by default
#
if __name__ == '__main__':
    entry_func()
