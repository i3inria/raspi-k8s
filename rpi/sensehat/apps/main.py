import sys

import display_color
import challenge
import display_image
import display_image2
import display_rotate_text
import display_text
import flip_image
import joy_led
import joy_led2
import joystick
import sense_acceleration
import sense_all
import sense_humid
import sense_orientation
import sense_pressure
import sense_temp
import shake
import show_letter
import show_letters
import show_random_letters

from sense_hat import SenseHat

sense = SenseHat()


def challenge_func():
    challenge.entry_func()
    return


def display_color_func():
    display_color.entry_func()
    return


def display_image_func():
    display_image.entry_func()
    return


def display_image2_func():
    display_image2.entry_func()
    return


def display_rotate_text_func():
    display_rotate_text.entry_func()
    return


def display_text_func():
    display_text.entry_func()
    return


def flip_image_func():
    flip_image.entry_func()
    return


def joy_led_func():
    joy_led.entry_func()
    return


def joy_led2_func():
    joy_led2.entry_func()
    return


def joystick_func():
    joystick.entry_func()
    return


def sense_acceleration_func():
    sense_acceleration.entry_func()
    return


def sense_all_func():
    sense_all.entry_func()
    return


def sense_humid_func():
    sense_humid.entry_func()
    return


def sense_orientation_func():
    sense_orientation.entry_func()
    return


def sense_pressure_func():
    sense_pressure.entry_func()
    return


def sense_temp_func():
    sense_temp.entry_func()
    return


def shake_func():
    shake.entry_func()
    return


def show_letter_func():
    show_letter.entry_func()
    return


def show_letters_func():
    show_letters.entry_func()
    return


def show_random_letters_func():
    show_random_letters.entry_func()
    return


MAX_APP = 20

switcher_func = {
    0: challenge_func,
    1: display_color_func,
    2: display_image_func,
    3: display_image2_func,
    4: display_rotate_text_func,
    5: display_text_func,
    6: flip_image_func,
    7: joy_led_func,
    8: joy_led2_func,
    9: joystick_func,
    10: sense_acceleration_func,
    11: sense_all_func,
    12: sense_humid_func,
    13: sense_orientation_func,
    14: sense_pressure_func,
    15: sense_temp_func,
    16: shake_func,
    17: show_letter_func,
    18: show_letters_func,
    19: show_random_letters_func,
}

switcher_message = {
    0: "challenge ",
    1: "led color ",
    2: "led image 1 ",
    3: "led image 2 ",
    4: "display_rotate_text_func",
    5: "display_text_func",
    6: "flip_image_func",
    7: "joy_led_func",
    8: "joy_led2_func",
    9: "joystick_func",
    10: "sense_acceleration_func",
    11: "sense_all_func",
    12: "sense_humid_func",
    13: "sense_orientation_func",
    14: "sense_pressure_func",
    15: "sense_temp_func",
    16: "shake_func",
    17: "show_letter_func",
    18: "show_letters_func",
    19: "show_random_letters_func",
}


blue = (0, 0, 255)
yellow = (255, 255, 0)


def launch_app():
    print ('service func')


if __name__ == '__main__':
    # service.py executed as script
    # do something
    if not len(sys.argv) == 2:
        print ('Usage: nb')
        sys.exit()

    current_app = 0

    sense.show_message(switcher_message[current_app], text_colour=yellow, back_colour=blue, scroll_speed=0.05)

    while True:

        for event in sense.stick.get_events():
            if event.action == 'pressed' and event.direction == 'middle':
                print (current_app)
                func = switcher_func.get(current_app, lambda: "Invalid app")
                func()

            if event.direction == 'up' or event.direction == 'right':
                current_app = current_app + 1
                if current_app == MAX_APP:
                    current_app = 0
                sense.show_message(switcher_message[current_app], text_colour=yellow, back_colour=blue,
                                   scroll_speed=0.05)

            if event.direction == 'down' or event.direction == 'left':
                current_app = current_app - 1
                if current_app == -1:
                    current_app = MAX_APP - 1
                sense.show_message(switcher_message[current_app], text_colour=yellow, back_colour=blue,
                                   scroll_speed=0.05)
