from sense_hat import SenseHat
from time import sleep

red = (255, 0, 0)
blue = (0, 0, 255)
green = (0, 255, 0)
white = (255, 255, 255)
yellow = (255, 255, 0)


# app main function
def entry_func():
    sense = SenseHat()

    sense.show_letter("L", red)
    sleep(1)
    sense.show_letter("a", blue)
    sleep(1)
    sense.show_letter("u", green)
    sleep(1)
    sense.show_letter("r", white)
    sleep(1)
    sense.show_letter("a", yellow)


#
#  entry point to launch entry_func by default
#
if __name__ == '__main__':
    entry_func()
