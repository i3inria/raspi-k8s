from sense_hat import SenseHat
from time import sleep

w = (150, 150, 150)
b = (0, 0, 255)
e = (0, 0, 0)

image = [
    e, e, e, e, e, e, e, e,
    e, e, e, e, e, e, e, e,
    w, w, w, e, e, w, w, w,
    w, w, b, e, e, w, w, b,
    w, w, w, e, e, w, w, w,
    e, e, e, e, e, e, e, e,
    e, e, e, e, e, e, e, e,
    e, e, e, e, e, e, e, e
]


# app main function
def entry_func():
    sense = SenseHat()

    sense.set_pixels(image)

    while True:
        sleep(1)
        sense.flip_h()


#
#  entry point to launch entry_func by default
#
if __name__ == '__main__':
    entry_func()
