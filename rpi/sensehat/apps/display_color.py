from sense_hat import SenseHat

r = 55
g = 25
b = 255


# app main function
def entry_func():
    sense = SenseHat()

    sense.clear((0, 0, 0))

    sense.clear((r, g, b))

    red = (255, 0, 0)
    sense.clear(red)


#
#  entry point to launch entry_func by default
#
if __name__ == '__main__':
    entry_func()
