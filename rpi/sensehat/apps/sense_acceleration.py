from sense_hat import SenseHat


# app main function
def entry_func():
    sense = SenseHat()

    while True:
        acceleration = sense.get_accelerometer_raw()
        x = acceleration['x']
        y = acceleration['y']
        z = acceleration['z']

        x = round(x, 0)
        y = round(y, 0)
        z = round(z, 0)

        print("x={0}, y={1}, z={2}".format(x, y, z))


#
#  entry point to launch entry_func by default
#
if __name__ == '__main__':
    entry_func()
