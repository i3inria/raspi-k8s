# -*- coding: latin-1 -*-
import smbus, time
bus = smbus.SMBus(1)

# initialisation du LSM9DS1
bus.write_byte_data(0x6a, 0x10, 0x0c3) #CTRL_REG1_G (0xc3)
bus.write_byte_data(0x6a, 0x22, 0x04) #CTRL_REG8 (0x04 -> IF_ADD_INC � '1')

# boucle de lecture pour calculer la compensation de l'�cart au z�ro
# attention, le sense hat doit rester immobile pendant cette phase
nbr_mesures = 100
tempo = 0.05
accu_x = 0
accu_y = 0
accu_z = 0
for a in range (nbr_mesures):
    # G axe Z
    data = bus.read_word_data(0x6a, 0x1c)
    if data >= 0x8000: data -= 65536
    accu_z += data
    # G axe Y
    data = bus.read_word_data(0x6a, 0x1a)
    if data >= 0x8000: data -= 65536
    accu_y += data
    # G axe X
    data = bus.read_word_data(0x6a, 0x18)
    if data >= 0x8000: data -= 65536
    accu_x += data
    time.sleep(tempo)

ecart_zero_x = accu_x/nbr_mesures
ecart_zero_y = accu_y/nbr_mesures
ecart_zero_z = accu_z/nbr_mesures

#lecture et affichage continue de Gz compens� de l'�cart au z�ro
while True:
    data = bus.read_word_data(0x6a, 0x18) # lecture du capteur Gx
    if data >= 0x8000: data -= 65536 #compl�ment � 2
    valeur_Gx = (data-ecart_zero_x)*245.0/(65536/2) # echelle de convertion

    data = bus.read_word_data(0x6a, 0x1a) # lecture du capteur Gy
    if data >= 0x8000: data -= 65536 # compl�ment � 2
    valeur_Gy = (data-ecart_zero_y)*245.0/(65536/2) # echelle de convertion

    data = bus.read_word_data(0x6a, 0x1c) #lecture du capteur Gz
    if data >= 0x8000: data -= 65536 #compl�ment � 2
    valeur_Gz = (data-ecart_zero_z)*245.0/(65536/2) #echelle de convertion

    print ("Gx :", valeur_Gx)
    print ("Gy :", valeur_Gy)
    print ("Gz :", valeur_Gz)
    time.sleep(0.5)
