from sense_hat import SenseHat

blue = (0, 0, 255)
yellow = (255, 255, 0)


# app main function
def entry_func():
    sense = SenseHat()

    while True:
        sense.show_message("Astro Pi is awesome!", text_colour=yellow, back_colour=blue, scroll_speed=0.05)


#
#  entry point to launch entry_func by default
#
if __name__ == '__main__':
    entry_func()
