from sense_hat import SenseHat


# app main function
def entry_func():
    sense = SenseHat()
    sense.clear()

    temp = sense.get_temperature()
    print(temp)


#
#  entry point to launch entry_func by default
#
if __name__ == '__main__':
    entry_func()
