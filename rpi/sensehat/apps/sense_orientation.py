from sense_hat import SenseHat


# app main function
def entry_func():
    sense = SenseHat()
    sense.clear()

    o = sense.get_orientation()
    pitch = o["pitch"]
    roll = o["roll"]
    yaw = o["yaw"]
    print("pitch {0} roll {1} yaw {2}".format(pitch, roll, yaw))


#
#  entry point to launch entry_func by default
#
if __name__ == '__main__':
    entry_func()
