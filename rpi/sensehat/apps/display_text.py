from sense_hat import SenseHat

red = (255, 0, 0)
green = (0, 255, 0)


# app main function
def entry_func():
    sense = SenseHat()

    sense.show_message("Hello world")
    sense.show_message("Another text", text_colour=red, back_colour=green)


#
#  entry point to launch entry_func by default
#
if __name__ == '__main__':
    entry_func()
