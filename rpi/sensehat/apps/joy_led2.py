from sense_hat import SenseHat


# Define the functions
def red():
    sense.clear(255, 0, 0)


def blue():
    sense.clear(0, 0, 255)


def green():
    sense.clear(0, 255, 0)


def yellow():
    sense.clear(255, 255, 0)


# app main function
def entry_func():
    sense = SenseHat()

    # Tell the program which function to associate with which direction
    sense.stick.direction_up = red
    sense.stick.direction_down = blue
    sense.stick.direction_left = green
    sense.stick.direction_right = yellow
    sense.stick.direction_middle = sense.clear  # Press the enter key

    while True:
        pass  # This keeps the program running to receive joystick events


#
#  entry point to launch entry_func by default
#
if __name__ == '__main__':
    entry_func()
