#! /usr/bin/python3
from sense_hat import SenseHat
from time import sleep
import csv
import time

sense = SenseHat()
# https://www.raspberrypi.org/forums/viewtopic.php?f=65&t=154050&sid=5415a769e06df885d19f6b70551fe2e4&start=25

with open('IMU_data.csv', mode='w+') as csv_file:
    fieldnames = ['ts','date','x','y','z','pitch','roll','yaw']
    writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
    writer.writeheader()

    # app main function
    def entry_func():
        sense = SenseHat()

        while True:

            start_ts = time.time()

            acceleration = sense.get_accelerometer_raw()
            orientation = sense.get_orientation()
            compas = sense.get_compass_raw()

            timestamp = time.time()
            now = time.strftime('%d-%m-%Y %H:%M:%S')

            x = acceleration['x']
            y = acceleration['y']
            z = acceleration['z']

            mx = compas['x']
            my = compas['y']
            mz = compas['z']

    #        x = round(x, 0)
    #        y = round(y, 0)
    #        z = round(z, 0)

            pitch = orientation["pitch"]
            roll = orientation["roll"]
            yaw = orientation["yaw"]

            # print("pitch {0} roll {1} yaw {2}".format(pitch, roll, yaw))
            # print("x={0}, y={1}, z={2}".format(x, y, z))
            # print("magnetic x: {x}, y: {y}, z: {z}".format(**raw))

            writer.writerow({
                'ts': timestamp,
                'date': now,
                'x': x,
                'y': y,
                'z': z,
                'pitch': pitch,
                'roll': roll,
                'yaw': yaw,
                'mx': mx,
                'my': my,
                'mz': mz
            })

            delay = time.time() - start_ts
            print ("delay {:.3f}".format(delay))

    #
    #  entry point to launch entry_func by default
    #
    if __name__ == '__main__':
        entry_func()
