from sense_hat import SenseHat

# Define some colours
g = (0, 255, 0)  # Green
b = (0, 0, 0)  # Black

# Set up where each colour will display
creeper_pixels = [
    g, g, g, g, g, g, g, g,
    g, g, g, g, g, g, g, g,
    g, b, b, g, g, b, b, g,
    g, b, b, g, g, b, b, g,
    g, g, g, b, b, g, g, g,
    g, g, b, b, b, b, g, g,
    g, g, b, b, b, b, g, g,
    g, g, b, g, g, b, g, g
]


# app main function
def entry_func():
    sense = SenseHat()

    # Display these colours on the LED matrix
    sense.set_pixels(creeper_pixels)


#
#  entry point to launch entry_func by default
#
if __name__ == '__main__':
    entry_func()
