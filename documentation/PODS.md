# Docker images and k8s Pods

This section described the creation/setup of various Docker images and Kubernetes deployments files. Deploying an MQTT broker is first detailed, with the Mosquitto broker as an example. 

This section targets Raspberry Pi deployments.

## MQTT
*   [https://projetsdiy.fr/mosquitto-broker-mqtt-raspberry-pi/](https://projetsdiy.fr/mosquitto-broker-mqtt-raspberry-pi/)
*   [https://philhawthorne.com/setting-up-a-local-mosquitto-server-using-docker-for-mqtt-communication/](https://philhawthorne.com/setting-up-a-local-mosquitto-server-using-docker-for-mqtt-communication/)
*   Cloud of mosquito [https://github.com/kairen/cloud-of-mosquitto](https://github.com/kairen/cloud-of-mosquitto)
*   Hyperbolic [https://github.com/hyperbolic2346/kubernetes/blob/master/mosquitto/mosquitto-deployment.yaml](https://github.com/hyperbolic2346/kubernetes/blob/master/mosquitto/mosquitto-deployment.yaml)

## Mosquitto

### RPI Docker image
*   based on raspbian//stretch
*   update/upgrade
*   Install repo/keys
*   Install mosquitto and mosquito client

Based on other Dockerfiles
*   [https://hub.docker.com/r/toke/mosquitto/](https://hub.docker.com/r/toke/mosquitto/) (how to start mqtt docker from systemd, but no conf)

Setup
*    [http://www.steves-internet-guide.com/creating-and-using-client-certificates-with-mqtt-and-mosquitto/](http://www.steves-internet-guide.com/creating-and-using-client-certificates-with-mqtt-and-mosquitto/)

Conf file
*   [http://www.steves-internet-guide.com/mossquitto-conf-file/](http://www.steves-internet-guide.com/mossquitto-conf-file/)
*   [https://hub.docker.com/_/eclipse-mosquitto/](https://hub.docker.com/_/eclipse-mosquitto/)
*   [https://ouilogique.com/mosquitto-mqtt-raspbery/](https://ouilogique.com/mosquitto-mqtt-raspbery/)

Testing
*   GUI clients [https://www.hivemq.com/blog/seven-best-mqtt-client-tools/](https://www.hivemq.com/blog/seven-best-mqtt-client-tools/)
*   CLI client [http://www.steves-internet-guide.com/mosquitto_pub-sub-clients/](http://www.steves-internet-guide.com/mosquitto_pub-sub-clients/)

Dockerfile
*   [https://github.com/pascaldevink/rpi-mosquitto](https://github.com/pascaldevink/rpi-mosquitto)


**Basic tests**

mosquitto_sub -h localhost -t test &

mosquitto_pub -h localhost -t test -m "hello world"

other testing tools [https://www.hivemq.com/blog/seven-best-mqtt-client-tools/](https://www.hivemq.com/blog/seven-best-mqtt-client-tools/)

(used mqttfx, fine)


### Scaling and load balancing



*   [https://github.com/kairen/cloud-of-mosquitto](https://github.com/kairen/cloud-of-mosquitto)
*   [https://github.com/kairen/cloud-of-mosquitto/blob/master/mosquitto-svc-bridge-image/Dockerfile](https://github.com/kairen/cloud-of-mosquitto/blob/master/mosquitto-svc-bridge-image/Dockerfile)

Can be achieved with nginx+ (pay) or haproxy (free)



*   [https://www.nginx.com/blog/nginx-plus-iot-load-balancing-mqtt/](https://www.nginx.com/blog/nginx-plus-iot-load-balancing-mqtt/)
*   (check link emqttd tutorial) [https://stackoverflow.com/questions/42810093/scaling-mosquitto-mqtt-to-more-than-2-servers](https://stackoverflow.com/questions/42810093/scaling-mosquitto-mqtt-to-more-than-2-servers)
*   [https://stackoverflow.com/questions/36283197/mqtt-mosquitto-bridge-horizontal-scaling](https://stackoverflow.com/questions/36283197/mqtt-mosquitto-bridge-horizontal-scaling)
*   

Many haproxy-mqtt docker



*   [https://qiita.com/giwa/items/5da37c3aca0aee75a628](https://qiita.com/giwa/items/5da37c3aca0aee75a628)
*   [https://github.com/lelylan/haproxy-mqtt](https://github.com/lelylan/haproxy-mqtt)

Better to use emqtt than mosquito for scalability



*   [https://www.sakib.ninja/how-to-setup-mqtt-cluster-with-haproxy/](https://www.sakib.ninja/how-to-setup-mqtt-cluster-with-haproxy/)

Other mqtt



*   Kafka: [https://dzone.com/articles/apache-kafka-mqtt-end-to-end-iot-integration-githu?utm](https://dzone.com/articles/apache-kafka-mqtt-end-to-end-iot-integration-githu?utm)
*   Emqtt [http://emqtt.io/](http://emqtt.io/)
*   

Bind mounts vs volumes: [https://docs.docker.com/v17.09/engine/admin/volumes/bind-mounts/#start-a-container-with-a-bind-mount](https://docs.docker.com/v17.09/engine/admin/volumes/bind-mounts/#start-a-container-with-a-bind-mount)

Good description of UID/GID issues in filesystems wrt hosts/containers

[https://medium.com/@nielssj/docker-volumes-and-file-system-permissions-772c1aee23ca](https://medium.com/@nielssj/docker-volumes-and-file-system-permissions-772c1aee23ca)

Docker list available images in private registry

check for images
```
curl https://pimaster.local:443/v2/_catalog
-> {"repositories":["busybox"]}
```
check for versions
```
curl https://pimaster.local:443/v2/busybox/tags/list
-> {"name":"busybox","tags":["default"]}
```
add check for https validity if wanted:` --cacert domain.crt`


### Kubernetes deployment

[https://github.com/hyperbolic2346/kubernetes/tree/master/mosquitto](https://github.com/hyperbolic2346/kubernetes/tree/master/mosquitto)


### K8s Dashboard access

Once deployed on kubernetes

connect to master and tunnel proxy port
```
ssh -L 8001:127.0.0.1:8001 pi@pimaster.local
```
on master, launch proxy (default port 8001)
```
kubectl proxy
```
on client computer, open browser at

[http://localhost:8001/api/v1/namespaces/kube-system/services/https:kubernetes-dashboard:/proxy/#!/service?namespace=default](http://localhost:8001/api/v1/namespaces/kube-system/services/https:kubernetes-dashboard:/proxy/#!/service?namespace=default)


## HiveMQ

*   [https://www.hivemq.com/blog/hivemq-cluster-docker-kubernetes/](https://www.hivemq.com/blog/hivemq-cluster-docker-kubernetes/)
*   

## Nodered

* Nodered install on docker: [https://nodered.org/docs/platforms/docker](https://nodered.org/docs/platforms/docker)

First install as standalone container (maps the docker port to the local port)
```
docker run -it -p 1880:1880 --name mynodered nodered/node-red-docker:rpi-v8
```
Open shell on container
```
docker exec -it mynodered /bin/bash
```
stop / start
```
docker stop mynodered
docker start mynodered
```
access the url from the navigator
```
pinode2.local:1880/#flow/4a8c1d44.6b6ba4
```

**Misc**

Create yaml for nodered based on redis description
apply the pod definition

connect ot the master, redirecting k8s’s nodered port

    ssh -L 31216:pimaster1.local:31216 pi@pimaster1.local

- access locally : [http://localhost:31216/](http://localhost:31216/)


## Node-RED

Node-RED
*   [https://nodered.org/docs/hardware/raspberrypi](https://nodered.org/docs/hardware/raspberrypi)
*   [https://nodered.org/docs/platforms/docker](https://nodered.org/docs/platforms/docker)
*   [https://github.com/elzekool/docker-rpi-nodered](https://github.com/elzekool/docker-rpi-nodered)

How to deploy 

    kubectl get secret -n kube-system | grep k8sadmin | cut -d " " -f1 | xargs -n 1 | xargs kubectl get secret  -o 'jsonpath={.data.token}' -n kube-system | base64 --decode
    cd 
    kubectl apply -f pod_nodered.yml
    kubectl get all
    kubectl get pods -o wide --all-namespaces
    kubectl get pods -o yaml
    kubectl get svc
    kubectl describe svc nodered
    kubectl expose deployment nodered --type=LoadBalancer --name=nodered-service
    kubectl get services nodered-service
    kubectl describe svc nodered-service
    kubectl get services nodered-service

manual for yaml/resources 

    kubectl api-resources
    kubectl explain pods
    kubectl explain pods.spec

    kubectl get services -o wide

    kubectl patch svc nodered-service -n nodered-service -p '{"spec": {"type": "LoadBalancer", "externalIPs":["192.168.0.70"]}}'

    kubectl get services -o wide

    curl 192.168.0.70:1880

Accessing the application

Exposing the nodered UI:



*   When deploying a Service in LoadBalancer mode, an External IP is assigned to it. For this, a special component in the Kubernetes cluster is required. While GCP and AWS provide suvh component, this is not the case for bare metal RPI cluster. In such case, the External IP for the Service can be set manually

Problems

*   When exposing a service in LoadBalancer mode, the External IP stays in &lt;pending> state. Reason: no automatic assignement of the IP address. Only on managed infra (aws, gcp). Need to manually assign the parameter.


## Redis

Complete Docker install and setup 
- [https://thisdavej.com/how-to-install-redis-on-a-raspberry-pi-using-docker/](https://thisdavej.com/how-to-install-redis-on-a-raspberry-pi-using-docker/) 
- [https://hub.docker.com/r/arm32v7/redis](https://hub.docker.com/r/arm32v7/redis)

Redis on arm
- [https://redis.io/topics/ARM](https://redis.io/topics/ARM)
- [https://habilisbest.com/install-redis-on-your-raspberrypi](https://habilisbest.com/install-redis-on-your-raspberrypi)

