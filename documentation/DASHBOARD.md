# Kubernetes Dashboard

## Install

Multiple install procedures are available:
- k3s and kubectl
- Arkade

### Install with k3s / kubectl

On the master node, create files for creating an admin user at install time:
- Create the file `dashboard.admin-user-role.yml`
with:
```
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: admin-user
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
subjects:
- kind: ServiceAccount
  name: admin-user
  namespace: kubernetes-dashboard
```
- Create the file `dashboard.admin-user.yml`
with:
```
apiVersion: v1
kind: ServiceAccount
metadata:
  name: admin-user
  namespace: kubernetes-dashboard
```

Then run the command

```
sudo k3s kubectl create -f dashboard.admin-user.yml -f dashboard.admin-user-role.yml
```

Then from the computer, create an ssh tunnel redirecting the port 8001 locally:

```
ssh -L 8001:127.0.0.1:8001 pi@pimaster1.local
```

In order to get the Dashboard token, on the master, run the following command
```
sudo kubectl -n kubernetes-dashboard describe secret $(sudo kubectl -n kubernetes-dashboard get secret | grep admin-user-token | awk '{print $1}')
```

and then on the master node, launch the proxy (this redirects the k8s API port to the local interface of the master (on 127.0.0.1)).

```
sudo kubectl proxy
```

Finally, open the web browser to access the Dashboard interface
 
http://127.0.0.1:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/

### Install with Arkade


## Links
K8s dashboard
* Online tutorial (safari only ?) [https://kubernetes.io/docs/tutorials/kubernetes-basics/create-cluster/cluster-interactive/](https://kubernetes.io/docs/tutorials/kubernetes-basics/create-cluster/cluster-interactive/)
* [https://kubernetes.io/docs/tutorials/kubernetes-basics/create-cluster/cluster-interactive/](https://kubernetes.io/docs/tutorials/kubernetes-basics/create-cluster/cluster-interactive/)
* [https://docs.giantswarm.io/guides/install-kubernetes-dashboard/](https://docs.giantswarm.io/guides/install-kubernetes-dashboard/)
* [https://kubecloud.io/kubernetes-dashboard-on-arm-with-rbac-61309310a640](https://kubecloud.io/kubernetes-dashboard-on-arm-with-rbac-61309310a640)

<p id="gdcalert5" ><span style="color: red; font-weight: bold">>>>>>  gd2md-html alert: inline image link here (to images/Edge-and4.png). Store image on your image server and adjust path/filename if necessary. </span><br>(<a href="#">Back to top</a>)(<a href="#gdcalert6">Next alert</a>)<br><span style="color: red; font-weight: bold">>>>>> </span></p>

![alt_text](images/Edge-and4.png "image_tooltip")

Remove nodes in K8s [https://stackoverflow.com/questions/35757620/how-to-gracefully-remove-a-node-from-kubernetes](https://stackoverflow.com/questions/35757620/how-to-gracefully-remove-a-node-from-kubernetes)

Pods in K8s

[https://kubernetes.io/docs/concepts/workloads/pods/pod/](https://kubernetes.io/docs/concepts/workloads/pods/pod/)


### Issue with dashboard 

*   [https://stackoverflow.com/questions/49457053/crashloopbackoff-on-kubernetes-dashboard](https://stackoverflow.com/questions/49457053/crashloopbackoff-on-kubernetes-dashboard)
*   Do the pastebin command
*   Check CIDR
*   issue with certificates (dashboard.crt)
*   [https://github.com/kubernetes/dashboard](https://github.com/kubernetes/dashboard) 
*   [https://github.com/kubernetes/dashboard/wiki/Installation](https://github.com/kubernetes/dashboard/wiki/Installation)
*   [https://github.com/kubernetes/dashboard/wiki/Installation#recommended-setup](https://github.com/kubernetes/dashboard/wiki/Installation#recommended-setup)
*   [https://github.com/kubernetes/dashboard/releases](https://github.com/kubernetes/dashboard/releases)

Certificate
*   [https://github.com/kubernetes/dashboard/wiki/Access-control#authorization-header](https://github.com/kubernetes/dashboard/wiki/Access-control#authorization-header)
*   Certificate management …

Error messages:
*   Error from server (AlreadyExists): secrets "kubernetes-dashboard-certs" already exists

### Creating dashboard.crt and certificates

[https://github.com/kubernetes/dashboard/wiki/Certificate-management](https://github.com/kubernetes/dashboard/wiki/Certificate-management)


```
cd
mkdir certs
cd certs
openssl genrsa -des3 -passout pass:xxxx -out dashboard.pass.key 2048
openssl rsa -passin pass:xxxx -in ./dashboard.pass.key -out dashboard.key
ls
openssl req -new -key dashboard.key -out dashboard.csr   
openssl x509 -req -sha256 -days 365 -in dashboard.csr -signkey dashboard.key -out dashboard.crt
```
**Error** : certificate folder not created/accessible
```
sudo mkdir /certs
sudo chmod a+rwx /certs
```
default location, not used - check config files in case
```
# cp dashboard.crt /certs/
```
create cert in k8s based on crt
```
kubectl create secret generic kubernetes-dashboard-certs --from-file=$HOME/certs -n kube-system
```

### Node specific deployments

Set labels for nodes and use node selectors in deployments

- set sensor hat true for pods
```
kubectl label nodes pi4node1.local "SensorHat=true"
```
Then in the spec section (i.e., not in containers)
```
    spec:
	nodeSelector:
		SensorHat: "true"
```



### Dashboard installation (ARM/RPI)

install arm dashboard
```
kubectl create -f https://raw.githubusercontent.com/kubernetes/dashboard/72832429656c74c4c568ad5b7163fa9716c3e0ec/src/deploy/recommended/kubernetes-dashboard-arm.yaml
```
If get error 
```
Error from server (AlreadyExists): error when creating "https://raw ….d-arm.yaml": secrets "kubernetes-dashboard-certs" already exists
```
delete all dashboard pods (dynamic)
```
kubectl -n kube-system delete $(kubectl -n kube-system get pod -o name | grep dashboard)
```
delete installed version of dashboard (install-file based, if that’s the one installed)
```
kubectl delete -f https://raw.githubusercontent.com/kubernetes/dashboard/master/aio/deploy/recommended/kubernetes-dashboard.yaml
```
and install version of dashboard again


```
kubectl create -f https://raw.githubusercontent.com/kubernetes/dashboard/72832429656c74c4c568ad5b7163fa9716c3e0ec/src/deploy/recommended/kubernetes-dashboard-arm.yaml
```

** Other error** : [https://superuser.com/questions/1254069/kubernetes-dashboard-not-working-already-exists-and-could-not-find-the-reque](https://superuser.com/questions/1254069/kubernetes-dashboard-not-working-already-exists-and-could-not-find-the-reque) 
```
kubectl delete -f https://raw.githubusercontent.com/kubernetes/dashboard/master/src/deploy/recommended/kubernetes-dashboard.yaml
```
check launch/progress SERVICES and PODS
```
kubectl get services -n kube-system
kubectl get pods -o wide --all-namespaces
kubectl logs --namespace kube-system kubernetes-dashboard-7df4f4cc8b-7bnvg
```

### Access dashboard UI

on the pimaster, kube proxy
```
kubectl proxy
```
from the client’s host (Mac), ssh tunneling with port redirect
```
ssh -L 8001:127.0.0.1:8001 pi@pimaster.local
```
then access the URL / all namespaces

[http://localhost:8001/api/v1/namespaces/kube-system/services/https:kubernetes-dashboard:/proxy/#!/overview?namespace=_all](http://localhost:8001/api/v1/namespaces/kube-system/services/https:kubernetes-dashboard:/proxy/#!/overview?namespace=_all)

[http://localhost:8001/api/v1/namespaces/kube-system/services/https:kubernetes-dashboard:/proxy/#!/overview?namespace=openfaas](http://localhost:8001/api/v1/namespaces/kube-system/services/https:kubernetes-dashboard:/proxy/#!/overview?namespace=openfaas)

[http://localhost:8001/api/v1/namespaces/kube-system/services/https:kubernetes-dashboard:/proxy/#!/deploy?namespace=default](http://localhost:8001/api/v1/namespaces/kube-system/services/https:kubernetes-dashboard:/proxy/#!/deploy?namespace=default)

[http://localhost:8001/api/v1/namespaces/kube-system/services/https:kubernetes-dashboard:/proxy/#!/overview?namespace=default](http://localhost:8001/api/v1/namespaces/kube-system/services/https:kubernetes-dashboard:/proxy/#!/overview?namespace=default)


### Access control for dashboard

[https://github.com/kubernetes/dashboard/wiki/Access-control#login-view](https://github.com/kubernetes/dashboard/wiki/Access-control#login-view)

**# Access control and login on dashboard**

[https://github.com/kubernetes/dashboard/wiki/Access-control](https://github.com/kubernetes/dashboard/wiki/Access-control)

blog at [https://8gwifi.org/docs/kube-dash.jsp](https://8gwifi.org/docs/kube-dash.jsp)

youtube at [https://www.youtube.com/watch?v=M6mHy0Cx2jE](https://www.youtube.com/watch?v=M6mHy0Cx2jE)

create the service account
```
kubectl create serviceaccount k8sadmin -n kube-system
```
bind to cluster role
```
kubectl  create clusterrolebinding k8sadmin --clusterrole=cluster-admin --serviceaccount=kube-system:k8sadmin
```
extract token and use in dashboard signin
```
kubectl get secret -n kube-system | grep k8sadmin | cut -d " " -f1 | xargs -n 1 | xargs kubectl get secret  -o 'jsonpath={.data.token}' -n kube-system | base64 --decode
```



## User authentication in K8S

[https://kubernetes.io/docs/reference/access-authn-authz/authentication/](https://kubernetes.io/docs/reference/access-authn-authz/authentication/)

[https://kubernetes.io/docs/tasks/access-application-cluster/configure-access-multiple-clusters/](https://kubernetes.io/docs/tasks/access-application-cluster/configure-access-multiple-clusters/)


## Connecting new node to K8S master

use the kubeadm join command from above

run with sudo

add `--ignore-preflight-errors=SystemVerification` 



## Troubleshooting

Flannel (NetworkPlugin cni) error: /run/flannel/subnet.env: no such file or directory

kube-flannel-ds-arm-lkgmx CrashLoopBackOff

Dashboard fails to start with dashboard.crt: no such file

Error from server (NotFound): secrets "kubernetes-dashboard-certs" not found

[ECDSAManager] Failed to open dashboard.crt for writing: open /certs/dashboard.crt: read-only file system

Unable to connect to the server: x509: certificate signed by unknown authority (possibly because of "crypto/rsa: verification error" while trying to verify candidate authority certificate "kubernetes")

failed to configure interface flannel  Remove additional addresses and try again

Get shell running in container

[https://kubernetes.io/docs/tasks/debug-application-cluster/get-shell-running-container/](https://kubernetes.io/docs/tasks/debug-application-cluster/get-shell-running-container/)


```
kubectl get pods

kubectl exec -it POD_NAME -- /bin/bash
```


Debug services

[https://kubernetes.io/docs/tasks/debug-application-cluster/debug-service/](https://kubernetes.io/docs/tasks/debug-application-cluster/debug-service/)



