# Networking




## Tunneling solutions

When web developers need to expose a private webapp on the Internet (testing , demo, ...), they usually deploy 
the application in a server running a DMZ, with some access control mechanism (e.g., Apache htpassword).
Overall, this requires a lot of devops and resources just for a simple test.

Alternatively, if some secure, Internet-accessible VMs are available in-house, ssh tunneling or a simple reverse-proxy 
(if a port can be open on the internal network for this VM) can be used to easily test a locally deployed app.
Here a [post](https://jerrington.me/posts/2019-01-29-self-hosted-ngrok.html) on how to use such VM with Nginx, 
Letsencrypt, and SSH reverse tunnelling.

While it needs less operations, it still requires a good understanding of networking and can suffer 
performance/reliability issues depending on the solution used. For example, it turns out that `kubectl port-forward` 
disconnects often and can give a poor connection for long-term use. 

To address these issues, various tunneling solutions have been developed.

**Warning**: Regarding these Cloud-based tunneling solutions, traffic is available to the cloud service, and 
opening a tunnel to the internal network is a security thread (e.g., typically, inadvertently leaking credentials 
in a commit).

### Ngrok

- Website: https://ngrok.com/
- Commercial/closed-source

Ngrok offers public URLs (e.g., http://12345.ngrok.io) that redirects traffic to an ngrok client running on a host, with a secure tunnel 
to the localhost network interface. Ngrok cloud service allows restricted access to the webapps exposed, as 
well as inspecting the traffic (e.g., for debugging) Ngrok use optimized tunneling for fast load, as well as websockets support and many other features.

### Inlets

- Website: https://inlets.dev/ or https://github.com/inlets
- Open-source/premium features

Developed within OpenFaaS, Inlets allows to connect HTTP and TCP services between networks securely. Through an 
encrypted websocket, inlets can penetrate firewalls, NAT, captive portals, and other restrictive networks lowering 
the barrier to entry. 

While Inlets can be used like ngrok, A use-case is being 

As explained [here](https://inlets.dev/blog/2021/04/13/local-port-forwarding-kubernetes.html), use-cases for Inlets 
(and other tunneling solutions) can be to do **remote port-forwarding** (making a webapp hosted internally available on a public VM for Internet use) or for local port-forwarding (making a remote service available on the Internet appear as a local service)
(apply to Inlets or other technologies)

<p align="center">
    <img src="/documentation/images/local-forwarding.jpg"  height="200">
    <img src="/documentation/images/remote-forwarding.jpg"  height="200">
</p>

### Sshuttle

TODO

### Other solutions, self-hosted "ngrok"

Many Cloud providers have secure tunneling features for hybrid-cloud support (public Cloud ressources + privately hosted resources). 
For example, Argo tunnels for Cloudflare): https://blog.cloudflare.com/tunnel-for-everyone/

There are also many solutions/discussions on how to have [self-hosted ngrok](https://pythonrepo.com/repo/anderspitman-awesome-tunneling-python-networking-programming)

Two examples: 
- https://baptiste.bouchereau.pro/tutorial/running-your-own-ngrok-with-docker/
- https://vitobotta.com/2019/06/29/self-hosted-alternative-to-ngrok/

