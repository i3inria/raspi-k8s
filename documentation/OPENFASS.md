# OpenFaaS for Serverless

OpenFaaS ([Website](https://www.openfaas.com/) and [Github](https://github.com/openfaas)) is a "FaaS/Serverless" solution that makes it easy for developers to deploy event-driven functions and microservices to Kubernetes without repetitive, boiler-plate coding.
It packages the function code in an OCI-compatible image to get a highly scalable endpoint with auto-scaling and metrics.

<p align="center">
    <img src="/documentation/images/openfaas_infra.png"  height="200">
</p>

**Note**: While primarily targeted at Kubernetes, OpenFaaS can also be deployed in a stand-alone fashion 
using [faasd](https://github.com/openfaas/faasd) for use-cases where a fully-fledge Kubernetes cluster is not 
required/available ([YouTube presentation](https://www.youtube.com/watch?v=ZnZJXI377ak) for the rational).

## Alternatives

[Here on CNCF](https://www.cncf.io/blog/2020/04/13/serverless-open-source-frameworks-openfaas-knative-more/) is a 
good presentation/comparison of serverless solutions (KNative, OpenFaaS, OpenWhisk, Firecracker & Oracle FN)

All major Cloud solutions provide serverless features
- Amazon [Lambda](https://aws.amazon.com/fr/lambda/)
- GCP [Cloud functions](https://cloud.google.com/functions)
- Scaleway [Serverless functions](https://www.scaleway.com/en/docs/scaleway-elements-serverless-getting-started/)
- ...

The main difference (advantage) is that Clouds' solution usually provide a complete UI experience enabling 
to copy/paste plain Python/Javascript/... functions while OpenFaaS requires already a Docker image.

Additional tools also exists for local deployement such as
- Fx [Github](https://github.com/metrue/fx)
- Iron function [Github](https://github.com/iron-io/functions)
- and others....

## Installation

### using Arkade

On the k8s' master (e.g., pimaster1 for cluster A)

```
export PATH=$PATH:$HOME/.arkade/bin/
export KUBECONFIG=/etc/rancher/k3s/k3s.yaml
```

Then insall with Arkade

```
sudo arkade install openfaas
```

Will display additional information (can be treieved later with `sudo arkade info openfaas`)

- to deploy the fass CLI
```
curl -SLsf https://cli.openfaas.com | sudo sh
```

- to forward the gateway to your machine
```
kubectl rollout status -n openfaas deploy/gateway
kubectl port-forward -n openfaas svc/gateway 8080:8080 &
```

- to log on the FaaS Gateway (UI)
```
# to get the command: sudo arkade info openfaas

OPENFAAS_PASSWORD=$(kubectl get secret -n openfaas basic-auth -o jsonpath="{.data.basic-auth-password}" | base64 --decode; echo)
echo -n $OPENFAAS_PASSWORD | faas-cli login --username admin --password-stdin
```

- and deploy figlet on the Raspberry
```
faas-cli store list  --platform armhf
faas-cli store deploy figlet  --platform armhf
```

## Accessing OpenFaaS

OpenFaas can be accessed thru its exposed UI or thru the CLI

To login, user is `admin` and password is retrieved with the command above 
```
PASSWORD=$(kubectl get secret -n openfaas basic-auth -o jsonpath="{.data.basic-auth-password}" | base64 --decode; echo)
```

### Gateway UI

In order to access it, ssh tunnel to the K8S master for port 31112 
```
ssh -L 31112:127.0.0.1:31112 pi@pimaster1.local
```
and open the local URL: http://localhost:31112/ui/

Or access directly http://pimaster1.local:31112/ui/

<p align="center">
    <img src="/documentation/images/openfaas_login.png"  height="200">
    <img src="/documentation/images/openfaas_deploy.jpeg"  height="200">
</p>


### openfaas-cli

Documentation for fass-cli is available on [Github](https://github.com/openfaas/faas-cli)

You can either use the CLI on the master, or from your own machine. In that case, First make sure k8s api is 
accessible with `kubectl proxy --port=8080` (can choose other port)

    brew install faas-cli

Get help

    faas-cli --help

Get help on a method

    faas-cli publish --help

Then can use the cli commands like 

    faas-cli store list

faas cli version and functions

    faas-cli version
    faas-cli list

list available methods for ARM/RPI

    faas-cli store list --yaml https://raw.githubusercontent.com/openfaas/store/master/store-armhf.json 

deploy meme function (alternatively, define the `OPENFAAS_URL` envvar for gateway URL)

    export OPENFAAS_URL=http://pimaster1.local:31112
    
    faas-cli store deploy mememachine  --gateway http://127.0.0.1:31112

## Deploying a method

Deploying faas functions is based on templates (for the various languages supported), extending them with the 
function's code and requirements, as well as a yaml fime for the deplopyment (image name, registry location, ...)

This can be done on the user's personal computer, building the Docker image on it and pushing it on DockerHub 
or a private registry, then issuing a deploy command to the gateway that fetches the image and start the contianer.

The detailed documentation for creation and deployment of functions is [here](https://docs.openfaas.com/cli/templates/).
It explains the differences beteween classic (stdio) vs of-watchdog (http) templates, Raspi vs the rest, as well as between up, 
build/push and publish options.

As explained [here](https://docs.openfaas.com/cli/templates/), there are differences for the arm/raspberry deployment process: "**For Raspberry Pi and ARM, you must use the publish command instead of build and push, or up.**"

### Using the CLI

- login to the FaaS gateway/UI (alternatively, use `--password-stdin``)

        faas-cli login --username admin --password $OPENFASS_TOKEN

- in a new folder `fns`, Fetch the desired language template and create the FaaS function (example of generated code available [here](./assets/openfaas))

        faas-cli new example --lang python

- update the `fns/example/handler.py` and `fns/example/requirements.txt` with the function's code and reqs

- update the `fns/example.yml` with the image name (`DOCKERHUB_ACCOUNT/example:latest`)

- create a token on the OockerHub account

- registers the openfaas cli on DockerHub

        faas-cli registry-login --username DOCKERHUB_ACCOUNT --password $DOCKERHUB_TOKEN

- build and publish the image

        faas-cli publish -f example.yml --platforms linux/arm/v7

- then the function can be deployed either from the UI (in the custom panel) or thru the the cli. The function is ready after a short time.

        faas-cli deploy --image=pgrav/openfaas_example:latest --name=example

- the method can de uninstalled using the UI or the CLI

        faas-cli delete example

### Using private Docker registries

By default DockerHub is used for pulling contianers' images.

When using a private registry (e.g., Inria's Gitlab instance), authentication is then required.

Many commands and configuration files are involved

#### Pulling images from private registry

Tutorials on deploying a private reigstry (plain docker, kubernetes, or full app)
- https://volkanpaksoy.com/archive/2019/08/10/Implementing-a-Self-Hosted-Docker-Registry-on-Raspberry-Pi/
- https://blog.alexellis.io/get-a-tls-enabled-docker-registry-in-5-minutes/
- https://thomas.inf3.ch/2017-08-27-openfaaskubernetes/index.html
- https://www.g2.com/categories/container-registry
- https://www.findbestopensource.com/product/atcol-docker-registry-ui
- https://github.com/goharbor/harbor
- https://goharbor.io/docs/2.0.0/install-config/quick-install-script/
- https://goharbor.io/docs/2.3.0/
- https://www.jpaul.me/2020/09/harbor-how-to-deploy-a-private-container-registry/

To launch docker containers from images in private registries, the user must first login to the registry 

    docker login registry.gitlab.inria.fr --username login@inria.fr --password=$GITLAB_TOKEN

The GITLAB_TOKEN is obtained on the 

This creates an authentication token for this registry in the `$HOME/.docker/config.json`file).

    {
            "auths": {
                    "registry.gitlab.inria.fr": {
                            "auth": "B64 ENCODED TOKEN="
                    }
            }
    }
secret in 


    sudo kubectl edit serviceaccount default -n openfaas-fn

    # Please edit the object below. Lines beginning with a '#' will be ignored,
    # and an empty file will abort the edit. If an error occurs while saving this file will be
    # reopened with the relevant failures.
    #
    apiVersion: v1
    imagePullSecrets:
    - name: inriagitlab
    kind: ServiceAccount
    metadata:
      creationTimestamp: "2021-08-06T22:28:49Z"
      name: default
      namespace: openfaas-fn
      resourceVersion: "488408"
      uid: 1beece9d-9da5-46fa-bfcf-7fb9b8491595
    secrets:
    - name: default-token-ccz4v

### CI/CD pipelines

Due to the fact that OpenFaaS functions are built into portable Docker images you can use any container builder 
to build your functions. The faas-cli can be used to build, push and deploy your functions.

Various solutions are explained in the OpenFaaS doc  [here](https://docs.openfaas.com/reference/cicd/intro/)
  
#### Using Github actions

A [simple tutorial](https://www.openfaas.com/blog/openfaas-functions-with-github-actions/)
 for deploying using Github actions.


#### Using Gitlab

A [simple tutorial](https://k33g.gitlab.io/articles/2019-09-16-OPENFAAS.html) for the Gitlab CI pipeline to build 
and deploy an OpenFaaS function.

And the [official doc](https://docs.openfaas.com/reference/cicd/gitlab/) with a simpler (less detailed) pipeline.

Access/authentication tokens on Gitlab are various
- Personal Access Token (from the user's `Preferences / Access Tokens` menu on the top/right corner)
- Project Access Tokens (from the project's `Settings / Access Tokens` menu on the project's page)

Projet Access Tokens are just Personal Access Token in disguise, with scoping limited to the specific project).

## Monitoring

The current OpenFaaS deployment already has Prometheus for monitoring and alerting.

In order to vizualize the performance of the cluster/openfaas, Grafana should be installed and configured

Grafana can either be deployed in the OpenFaaS namespace on Kubernetes, thus accessing directly Prometheus, or 
can be deployed externally to Kubernetes (as a simple Docker container) , then connecting to the redirected datasources.
This Grafana can then be used to monitor/visualize various environments (not just OpenFaaS).

### Installation

SSH on the k8s master (pimaster1.local)

- Install using Arkade in the same namespace as OpenFaaS

    sudo arkade install grafana -n openfaas

- Info about the grafana commands can always be retrieved with 
    
    sudo arkade info grafana

- Redirect the grafana service port (80) on the cluster 3000 port (this means Grafana can be accessed on the 127.0.0.1:3000 address on the master). Check it is accessible.

    # redirects to 127.0.0.1 only by default
    sudo kubectl --namespace openfaas port-forward service/grafana 3000:80

    # redirects on all interfaces (or specify which one explicitely)
    sudo kubectl --namespace openfaas port-forward service/grafana 3000:80  --address 0.0.0.0

- In another terminal on master, chack grafana availability

    wget 127.0.0.1:3000

- Then retrieve the admin password to sign in later

    sudo kubectl get secret --namespace openfaas grafana -o jsonpath="{.data.admin-password}" | base64 --decode ; echo

- With ssh tunneling, redirects the port 3000 on the local machine 

    ssh -L 3000:127.0.0.1:3000 pi@pimaster1.local

        
- Access the Grafana UI on http://localhost:3000, and login using the admin login and password
- Or if the master is accessible and port forwarded on all interfaces, access http://pimaster1.local:3000

- Port redirection (kubectl proxy or port-forwar- [official doc on port forward](https://kubernetes.io/docs/tasks/access-application-cluster/port-forward-access-application-cluster/)) are sensitive to disconnection/timeouts, which often 
happens (e.g., Pod restart). In order to have a more reliable port forwarding, it is thus required to encapsulate 
these instructions in a shell script that loops/check connections (check
 [here](https://stackoverflow.com/questions/47484312/kubectl-port-forwarding-timeout-issue?rq=1) 
 or to use [NodePort](https://stackoverflow.com/questions/60019750/getting-a-connection-timeout-issue-with-port-forwarding-in-kubernetes?noredirect=1&lq=1) for solutions))

- Create a datasource named Prometheus (in configuration/datasource) with http://prometheus:9090 as URL for the datasource.

<p align="center">
    <img src="/documentation/images/grafana_datasource.png"  height="200">
    <img src="/documentation/images/grafana_datasource2.png"  height="200">
</p>
- Create a dashboard and import a template from GrafanaLabs https://grafana.com/grafana/dashboards/3526, configure the prometheus datasource.

<p align="center">
    <img src="/documentation/images/grafana_import.png"  height="200">
    <img src="/documentation/images/grafana_dashboard.png"  height="200">
</p>

### Links 

- A [tutorial](https://6figuredev.com/learning/play-with-docker-openfaas-net-core-and-grafana/) using Grafana as an external Docker (not in k8s)
- Monitoring function [examples](https://docs.openfaas.com/architecture/metrics/)
- Another GrafanaLabs template: https://grafana.com/grafana/dashboards/3434



## PacketLab IoT Workshop

Conceptual architecture of a demo for Drone deliveries monitpring [demoed at CES](https://twitter.com/alexellisuk/status/1215280064797069312)

Link to [Github](https://github.com/packet-labs/iot)

<p align="center">
    <img src="/documentation/images/openfaas_infra.png"  height="100">
</p>

## Additional links 

Introductions
* OpenFaaS Getting started (https://ericstoekl.github.io/faas/getting-started/faas-cli/)
* Managing images (https://ericstoekl.github.io/faas/operations/managing-images/)

Creating/deploying functions
* complete tutorial https://medium.com/analytics-vidhya/serverless-with-openfaas-kubernetes-and-python-9934e4a80de5

*   [https://github.com/openfaas/faas](https://github.com/openfaas/faas)

OpenFaaS with Kubernetes
*   [https://docs.openfaas.com/deployment/kubernetes/](https://docs.openfaas.com/deployment/kubernetes/)
*   [https://github.com/openfaas/faas-netes](https://github.com/openfaas/faas-netes)

OpenFaaS, K8S, RasPi
*   [https://medium.com/@JockDaRock/serverless-at-the-edge-up-and-running-w-openfaas-docker-on-a-raspberry-pi-multi-node-cluster-e0957f4d8a49](https://medium.com/@JockDaRock/serverless-at-the-edge-up-and-running-w-openfaas-docker-on-a-raspberry-pi-multi-node-cluster-e0957f4d8a49)

## Deprecated 

### Install using Helm (older install)

On the k8s' master (e.g., pimaster1 for cluster A)

```
cd
mkdir openfaas
cd openfaas/
curl -sL https://cli.openfaas.com | sudo sh
```
will output
```
armv7l
Downloading package https://github.com/openfaas/faas-cli/releases/download/0.8.6/faas-cli-armhf as /tmp/faas-cli-armhf
Download complete.

Running as root - Attempting to move faas-cli to /usr/local/bin
New version of faas-cli installed to /usr/local/bin
Creating alias 'faas' for 'faas-cli'.
  ___               	_____       	____
 / _ \ _ __   ___ _ __ |  ___|_ _  __ _/ ___|
| | | | '_ \ / _ \ '_ \| |_ / _` |/ _` \___ \
| |_| | |_) |  __/ | | |  _| (_| | (_| |___) |
 \___/| .__/ \___|_| |_|_|  \__,_|\__,_|____/
  	|_|

CLI:
 commit:  96f159fc3e85a6782461c9dde77cbf88308aa21d
 version: 0.8.6
```

Then deploy openfaas via Helm

[https://github.com/openfaas/faas-netes/blob/master/HELM.md](https://github.com/openfaas/faas-netes/blob/master/HELM.md)

First install helm
```
curl https://raw.githubusercontent.com/kubernetes/helm/master/scripts/get | bash
```
create RBAC permissions for Tiller (service account/role binding)
```
kubectl -n kube-system create sa tiller  && kubectl create clusterrolebinding tiller --clusterrole cluster-admin --serviceaccount=kube-system:tiller
```
Install the server-side Tiller component on your cluster
```
helm init --skip-refresh --upgrade --service-account tiller

Creating /home/pi/.helm
Creating /home/pi/.helm/repository
Creating /home/pi/.helm/repository/cache
Creating /home/pi/.helm/repository/local
Creating /home/pi/.helm/plugins
Creating /home/pi/.helm/starters
Creating /home/pi/.helm/cache/archive
Creating /home/pi/.helm/repository/repositories.yaml
Adding stable repo with URL: https://kubernetes-charts.storage.googleapis.com
Adding local repo with URL: http://127.0.0.1:8879/charts
$HELM_HOME has been configured at /home/pi/.helm.

Tiller (the Helm server-side component) has been installed into your Kubernetes Cluster.

Please note: by default, Tiller is deployed with an insecure 'allow unauthenticated users' policy.
To prevent this, run `helm init` with the --tiller-tls-verify flag.
For more information on securing your installation see: https://docs.helm.sh/using_helm/#securing-your-helm-installation
Happy Helming!
```

**tiller pod crashloopbackoff - how to delete/remove tiller

[https://stackoverflow.com/questions/47583821/how-to-delete-tiller-from-kubernetes-cluster](https://stackoverflow.com/questions/47583821/how-to-delete-tiller-from-kubernetes-cluster)

```
helm reset --force
```
