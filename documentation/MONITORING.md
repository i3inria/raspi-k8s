# Monitoring a Kubernetes cluster


## Components and architecture

## Tools 

- service collectors (to collect monitoring info on echa node)
- prometheus as the central collector of metrics/monitoring info
- grafana as the visualizer. This connects to prometeus to get statistics, and display then in various grafana dashboards installed separatedly
- promotheus operator (optional) as an extention of the kubernetes API to do promeeus install/config the k8s way

### Arkade 

arkade helps installing tools and apps

https://github.com/alexellis/arkade


## Nodes and pods monitoring
 
### cAdvisor

https://github.com/google/cadvisor


### 

## Logs collection

## Visualisation

### Grafana

### Dashboards

#### Kubernetes cluster monitoring

https://grafana.com/grafana/dashboards/315


#### Node exporter dashboard

https://grafana.com/grafana/dashboards/1860

#### Alternate NodeExporter dashboard

https://grafana.com/grafana/dashboards/13978?pg=dashboards&plcmt=featured-sub1

## Tutorials

### NodeExporter quickstart

https://grafana.com/oss/prometheus/exporters/node-exporter/

### Monitoring section on rpi4cluster

https://rpi4cluster.com/monitoring/monitor-intro/


### ELK/EFK, Promethus/Grafana, Telegraf/grafan tutorial and comparison

https://betterprogramming.pub/kubernetes-application-monitoring-on-a-raspberry-pi-cluster-fa8f2762b00c?gi=ff7ee5ea9c39

### Reliable Kubernetes on a Raspberry Pi Cluster

5 parts serie

- storage https://medium.com/codex/reliable-kubernetes-on-a-raspberry-pi-cluster-storage-ff2848d331df
- monitoring https://medium.com/codex/reliable-kubernetes-on-a-raspberry-pi-cluster-monitoring-a771b497d4d3
