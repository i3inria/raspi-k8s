# Setting up a Raspberry Pi Cluster

In this document, we describe how to setup a cluster of RPis (os, networking, ...) so that Kubernetes can be 
installed on it afterwards.

ALL THESE STEPS ARE AVAILABLE AS ANSIBLE PLAYBOOK [HERE](../cluster/README.md) (steps 0_sdcard and 1_system).

## Hardware

Below is a personal bill of materials for a RPi cluster. 

**Disclaimer** For convenience, all links point to some products on one major ecommerce platform but it is just for
reference (not an endorsement the ecommerce platform or the specific products/vendors.

Required hardware
- RPi 3 or 4 (at least 3) - [here](https://www.amazon.fr/Raspberry-Mod%C3%A8le-ARM-Cortex-A72-WLAN-AC-Bluetooth/dp/B07TC2BK1X/ref=sr_1_3?__mk_fr_FR=%C3%85M%C3%85%C5%BD%C3%95%C3%91&keywords=raspberry+pi+4&qid=1583850221&sr=8-3)
- MicroSD cards (class 10) - - [here](https://www.amazon.fr/Samsung-MB-MC128GA-EU-m%C3%A9moire-Adaptateur/dp/B06XFWPXYD/ref=sr_1_3?__mk_fr_FR=%C3%85M%C3%85%C5%BD%C3%95%C3%91&keywords=carte%2Bmicro%2Bsd%2B128gb&qid=1583850851&sr=8-3&th=1)
- USB power bloc or individual power - [here](https://www.amazon.fr/AmazonBasiscs-5-Port-Charger-USB-C-Delivery/dp/B07PCLTY4H/ref=sr_1_14?__mk_fr_FR=%C3%85M%C3%85%C5%BD%C3%95%C3%91&keywords=usb%2Bchargeur%2B5%2Bports&qid=1583850466&sr=8-14&th=1)
- Stackable cases and relevant bolts/standoff - [here](https://www.amazon.fr/GeeekPi-Raspberry-Ventilateur-Dissipateur-Acrylique/dp/B07Z4GRQGH/ref=sr_1_4?__mk_fr_FR=%C3%85M%C3%85%C5%BD%C3%95%C3%91&keywords=raspberry+pi+case+stack&qid=1583852363&sr=8-4)

Recommended hardware
- Short USB cables (if using multi-USB power bloc) - [here](https://www.amazon.fr/dp/B0732QY5DG/ref=sspa_dk_detail_2?psc=1&pd_rd_i=B0732QY5DG&pd_rd_w=gC4w7&pf_rd_p=42b0c882-2f60-488f-b868-d46b19aecf92&pd_rd_wg=svAUd&pf_rd_r=HK14CZAZ2XRFNTFVGCZ8&pd_rd_r=93c683ea-89ac-427e-b1fc-89c5c0bff3f0&spLa=ZW5jcnlwdGVkUXVhbGlmaWVyPUFUMFVXQjNZVUxIQ1QmZW5jcnlwdGVkSWQ9QTA0NjMyNzRSVjU1M01UMTM1WFEmZW5jcnlwdGVkQWRJZD1BMDkwMTQ2NDJEUEpDWUU4S0JTRDYmd2lkZ2V0TmFtZT1zcF9kZXRhaWwmYWN0aW9uPWNsaWNrUmVkaXJlY3QmZG9Ob3RMb2dDbGljaz10cnVl)
or [here](https://www.amazon.fr/MaGeek-Paquet-Vitesse-Samsung-Motorola/dp/B00WMAQKS2/ref=pd_sim_147_3/258-2230758-0010816?_encoding=UTF8&pd_rd_i=B00WMAQKS2&pd_rd_r=56cab06b-60c9-408e-a02d-4cee1908c866&pd_rd_w=yscVk&pd_rd_wg=ezCoa&pf_rd_p=13666dbd-eacc-4961-ae51-8b11476bba29&pf_rd_r=E7NMZ4DX6JK2C9N7ZB5D&psc=1&refRID=E7NMZ4DX6JK2C9N7ZB5D)
- Short flat ethernet cables - [here](https://www.amazon.fr/r%C3%A9seau-Gigabit-Ethernet-compatible-7-Farben/dp/B077DBWL83/ref=pd_sbs_147_7?_encoding=UTF8&pd_rd_i=B077DBWL83&pd_rd_r=68640706-e18a-43e1-abbd-c3c926abaece&pd_rd_w=VV2tv&pd_rd_wg=nVq9y&pf_rd_p=72159c7a-2bb2-4a15-aa35-b315ce8f5c64&pf_rd_r=C7BE0CJPP0RFSNX4SKMX&refRID=C7BE0CJPP0RFSNX4SKMX&th=1)
- Wireless router - [here](https://www.amazon.fr/TP-Link-Routeur-R%C3%A9p%C3%A9teur-Ethernet-TL-WR902AC/dp/B00TQEX8BO/ref=sr_1_6?__mk_fr_FR=%C3%85M%C3%85%C5%BD%C3%95%C3%91&keywords=usb%2Bpower%2Bwifi%2Brouter&qid=1583851559&sr=8-6&th=1)
- Ethernet switch - [here](https://www.amazon.fr/GS305-300PES-Ethernet-Connectivit%C3%A9-Abordable-Entreprises/dp/B07PTMXBDK/ref=sr_1_1_sspa?__mk_fr_FR=%C3%85M%C3%85%C5%BD%C3%95%C3%91&keywords=usb+ethernet+switch+5+ports+rj45&qid=1583849503&sr=8-1-spons&psc=1&spLa=ZW5jcnlwdGVkUXVhbGlmaWVyPUEzRThJMDQ1VEpPSFJQJmVuY3J5cHRlZElkPUExMDQyNzcxMk0xTzdQMjk3REFRRiZlbmNyeXB0ZWRBZElkPUEwNDYwODg1M01DR1JTSDhHR1NFOCZ3aWRnZXROYW1lPXNwX2F0ZiZhY3Rpb249Y2xpY2tSZWRpcmVjdCZkb05vdExvZ0NsaWNrPXRydWU=)
 
## Initial RPi Installation

The first objective is to have a basic setup of all the boards that we can access from the admin's host (PC, Mac) via ssh,
so that we can then install all the system components we require.

Once the RPi boards have been properly mounted in the cases, the OS must first be flashed on the micro-SD card. Then the preliminary OS configuration steps can either 
be done interactively (this requires having screen/keyboard/mouse pluged in to the RPi), or we can go thru a "headless" 
install (preferred method).

A detailed doc for these initial ssh/networking steps 
is available [here](https://desertbot.io/blog/headless-raspberry-pi-4-ssh-wifi-setup)

### SD card preparation

In order to launch Raspeberry Pi OS (formelmy Raspbian) on a Raspberry Pi, a first step is to flash the OS on a micro-SD card.

*Step 1*: download tool to flash and install [BalenaEtcher](https://www.balena.io/etcher/) (also available for Win/Linux)

*Step 2*: download OS image, [current version](https://www.raspberrypi.org/downloads/raspberry-pi-os/)
- Raspberry OS
- Release date: May 7th 2021
- Kernel version: 5.10

*Step 3*: flash on SD card

**Warning**: Do not switch sdcard/board afterwards in a cluster as the RPi boards do not the same MAC addresses, and this may introduce
inconsistencies in the cluster's configuration.

**Side topic:** here a few links on flasshing RPi live [here](https://raspberrypi.stackexchange.com/questions/26578/is-it-possible-to-flash-the-pis-firmware-live) 
and [here](https://raspberrypi.stackexchange.com/questions/37920/how-do-i-set-up-networking-wifi-static-ip-address)

**Side topic:** about SD card [backup]([https://thepihut.com/blogs/raspberry-pi-tutorials/17789160-backing-up-and-restoring-your-raspberry-pis-sd-card](https://thepihut.com/blogs/raspberry-pi-tutorials/17789160-backing-up-and-restoring-your-raspberry-pis-sd-card))


### Headless install

For a headless install, follow the steps

*Step 1*: eject/insert the SD card in the PC/Mac after flashing the OS, and mount as a normal disk (this mounts a small FAT32 partition)

*Step 2*: to enable ssh, create `ssh` file at root of mounted boot partition (file can be empty and will be deleted 
automatically afterwards)

*Step 3*: by default, networking is enabled for ethernet. To enable Wifi, the proper configuration file should also be 
created on the boot partition.

Create `wpa_supplicant.conf` file at the root of the mounted boot partition and add the following content (change 
according to your configuration)

```
country=FR
ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
update_config=1

network={
    ssid="NETWORK-NAME"
    psk="NETWORK-PASSWORD"
}
```

**Warning:** WiFi setup requires country to be set.

Then install on the RPi and boot it up. 

Skip interactive install if steps above done.

### Interactive install

Insert the micro-SD card in the RPi and turn on, wait for Raspbian to launch, then can use the command line, the 
admin settings tools, or the **raspi-config** tool (preferred method).

With `raspi-config`, setup the country/langage, networking, ssh access. 

If using the terminal/command line, enable ssh with:
```
sudo apt-get -y install ssh
sudo /etc/init.d/ssh start
sudo update-rc.d ssh defaults
```

## Initial OS Setup

By default the RPi will try to get an IP address from DHCP server, and if none is available, an IP address is 
automatically assigned. It is then possible to connect to the device with the IP address (if known) or using 
the hostname (each RPi broadcasts its hostname so that name resolution can be done in the local network).
We assume your host (Mac/PC) is connected on the same network (Ethernet or WiFi).

**[Avahi](https://www.avahi.org/)** is the Linux service that performs network-based Zeroconf service discovery 
(DNS-SD and multicast-DNS service discovery). The avahi daemon is installed by default and starts on boot.
This service enables connecting to a device on the local network using HOSTNAME.local (i.e., it resolves the IP 
address  of the host based on its name).

It is then possible to ssh to the RPi using the default credentials (user `pi` pwd `pi`).

### Hostname and default user 

From the main host (Mac/PC), connect to the RPIs using `ssh` (default password is `raspberry`)

    ssh pi@raspberrypi.local

*Step 1*: Change the hostname `raspberrypi` with a new unique hostname for the cluster (pinode1, pinode2, pimaster...)
using the following methods:

    sudo nano /etc/hostname
    sudo nano /etc/hosts

or 

    sudo raspi-config
    
then reboot

    sudo reboot

**Side topic:** [Issue](https://serverfault.com/questions/652102/how-to-prevent-avahi-adding-2-to-hostnames) with 
hostnames locally assigned with -2 or -3 suffixes over the network

*Step 2*: ssh again to the RPi using the new hostname and change password of pi user

    sudo passwd

**Important:** Use the same password on all the boards as the Ansible scripts assume same credentials used on all 
the RPIs.

```ssh-keygen -f "path/to/.ssh/known_hosts" -R "hostname"```

### Authorize direct ssh between cluster nodes

On the host machine, generate a public and a private key to enable ssh connections without entering the password each 
time ([info](https://www.ssh.com/ssh/keygen))

    ssh-keygen -f ~/key-rpi-cluster -t rsa -b 4096

On the RPi, create the `.ssh` folder (**IMPORTANT: restrict access**) and create the file `~/.ssh/authorized_keys` 
that contains the public key for the defined cluster.

### Packages install 

Update and install needed packages
```
sudo apt-get -y update
sudo apt-get -y install build-essential git-core screen tmux ufw telnet curl network-manager avahi-daemon vim dnsutils
mkdir git
```

## Networking

In this section, we setup the networking configuration for the cluster, assigning static IP addresses for all nodes 
in the different environments targeted for experimentation (e.g., work, home) 

Setting up networking is tricky and error prone. 

A few useful references:

- Various ways to disable wifi/bluetooth
https://pimylifeup.com/raspberry-pi-disable-wifi/


### Static IP addresses

setup static IP address - required for cluster

-uncomment/update the eth0 section to wlan0

read about networking there

[https://raspberrypi.stackexchange.com/questions/37920/how-do-i-set-up-networking-wifi-static-ip-address/74428#74428](https://raspberrypi.stackexchange.com/questions/37920/how-do-i-set-up-networking-wifi-static-ip-address/74428#74428)

update hostname also
```
vim /etc/dhcpcd.conf
```
check dhcpcd active
```
sudo service dhcpcd status
```
to activate if needed
```
sudo service dhcpcd start
sudo systemctl enable dhcpcd
```
with avahi daemon (can use hostname for ssh)
```
ssh pi@pimaster.local
```

### Setting up IP addresses 

[https://raspberrypi.stackexchange.com/questions/37920/how-do-i-set-up-networking-wifi-static-ip-address](https://raspberrypi.stackexchange.com/questions/37920/how-do-i-set-up-networking-wifi-static-ip-address)

In particular static IP addresses

[https://raspberrypi.stackexchange.com/questions/37920/how-do-i-set-up-networking-wifi-static-ip-address/74428#74428](https://raspberrypi.stackexchange.com/questions/37920/how-do-i-set-up-networking-wifi-static-ip-address/74428#74428)

More on [setting up wifi from the command line](https://www.raspberrypi.org/documentation/configuration/wireless/wireless-cli.md)

### Networking and DHCP

Docker and Kubernetes requires a static IP address, at least for the master node. This static IP address should be set during installation. Also, the gateway and DNS servers may differ depending on the location/network.
- At Inria:
- At home, using ADSL provider:

To setup a static IP address, the following links were useful
- [https://www.raspberrypi.org/forums/viewtopic.php?f=91&t=124423&sid=66f47f6c9afec7cfb3414e6475b4b7f1](https://www.raspberrypi.org/forums/viewtopic.php?f=91&t=124423&sid=66f47f6c9afec7cfb3414e6475b4b7f1)
- [https://www.domo-blog.fr/configurer-adresse-ip-static-raspbian-stretch/](https://www.domo-blog.fr/configurer-adresse-ip-static-raspbian-stretch/)
- [https://www.raspberrypi.org/forums/viewtopic.php?t=140252](https://www.raspberrypi.org/forums/viewtopic.php?t=140252)
- [https://www.raspberrypi.org/forums/viewtopic.php?t=84963](https://www.raspberrypi.org/forums/viewtopic.php?t=84963) 

Issue about the static IP in /etc/dhcpcd.conf not taking effect:
- The configuraton is not properly written
- There is a conflict with dhcpcd and dhclient both active
- Interface not properly set if currently used for ssh
- Files modification date wrt dhclient and if-up/down scripts (detecting changes)

dhclient/networkmanager/dhcpcd conflicts

- [https://debian-facile.org/viewtopic.php?id=21340](https://debian-facile.org/viewtopic.php?id=21340)
- [https://www.raspberrypi.org/forums/viewtopic.php?t=140045](https://www.raspberrypi.org/forums/viewtopic.php?t=140045)
- [https://askubuntu.com/questions/459140/why-dhclient-is-still-running-when-i-choose-static-ip](https://askubuntu.com/questions/459140/why-dhclient-is-still-running-when-i-choose-static-ip)
- [https://serverfault.com/questions/148443/disable-dhcp-client-over-one-interface](https://serverfault.com/questions/148443/disable-dhcp-client-over-one-interface)
- [https://raspberrypi.stackexchange.com/questions/39785/differences-between-etc-dhcpcd-conf-and-etc-network-interfaces](https://raspberrypi.stackexchange.com/questions/39785/differences-between-etc-dhcpcd-conf-and-etc-network-interfaces)
- [https://www.raspberrypi.org/forums/viewtopic.php?t=191678](https://www.raspberrypi.org/forums/viewtopic.php?t=191678)
- [https://www.raspberrypi.org/forums/viewtopic.php?t=221636](https://www.raspberrypi.org/forums/viewtopic.php?t=221636)
- [https://debian-facile.org/viewtopic.php?id=21340](https://debian-facile.org/viewtopic.php?id=21340)
- [https://raspberrypi.stackexchange.com/questions/39785/differences-between-etc-dhcpcd-conf-and-etc-network-interfaces](https://raspberrypi.stackexchange.com/questions/39785/differences-between-etc-dhcpcd-conf-and-etc-network-interfaces)

Since Debian Jessie Raspbian, using `dhcpcd.conf` instead of `/etc/network/interfaces`. However, is avahi-daemon or NetworkManager are installed/used, they will refer to the `/etc/network/interfaces` file and launch dhclient, thus assigning dhcp address (or multiple addresses) if nothing is configured.

Proper way:
- Setup DHCP server to provide the defined static IP address based on the MAC address (works for both dhclient and dhcpcd)

Alternative way
- Disable eth0 in `/etc/network/interfaces`.

checking the logs for DHCP
```
journalctl | grep -Ei 'dhcp'
```
or
```
cat /var/log/syslog | grep -Ei 'dhcp'.
```
check status of NetworkManager and network services
```
systemctl status NetworkManager.service
systemctl status network.service
```
disable NetworkManager (relies on dhclient and has conflict with dhcpcd
```
systemctl stop NetworkManager.service
systemctl disable NetworkManager.service
```
enable at least network service (?) but does not exist or Raspbian
```
systemctl enable network.service 
systemctl start network.service
```
disable in `/etc/network/interfaces`.
```
sudo vi /etc/network/interfaces
```
and add
```
auto lo
iface lo inet loopback

iface eth0 inet manual
```

### Set RPi as WiFi hotspot

Used
*   [https://www.raspberrypi.org/documentation/configuration/wireless/access-point.md](https://www.raspberrypi.org/documentation/configuration/wireless/access-point.md)

Plus corrections in 
*   [https://www.raspberrypi.org/forums/viewtopic.php?t=235598](https://www.raspberrypi.org/forums/viewtopic.php?t=235598)
*   [https://www.raspberrypi.org/forums/viewtopic.php?t=217393](https://www.raspberrypi.org/forums/viewtopic.php?t=217393)

But not really here
*   [https://www.qlcplus.org/forum/viewtopic.php?t=12468](https://www.qlcplus.org/forum/viewtopic.php?t=12468)

install/update system
```
sudo apt-get update
sudo apt-get upgrade
```
install hotspot packages, disable and reboot
```
sudo apt-get install dnsmasq hostapd
sudo systemctl stop dnsmasq
sudo systemctl stop hostapd
sudo reboot
```
edit interfaces config
```
sudo nano /etc/dhcpcd.conf
```
and add at the end
```
interface wlan0
    static ip_address=192.168.4.1/24
    nohook wpa_supplicant
```
restart
```
sudo service dhcpcd restart
```
edit dns config
```
sudo mv /etc/dnsmasq.conf /etc/dnsmasq.conf.orig  
sudo nano /etc/dnsmasq.conf
```
and add (change range and network addr accordingly
```
interface=wlan0      # Use the require wireless interface - usually wlan0
  dhcp-range=192.168.4.2,192.168.4.20,255.255.255.0,24h
```
edit hotspot config
```
sudo nano /etc/hostapd/hostapd.conf
```
and add (**additionally added country code**)
```
interface=wlan0
driver=nl80211
country_code=FR
ssid=NameOfNetwork
hw_mode=g
channel=7
wmm_enabled=0
macaddr_acl=0
auth_algs=1
ignore_broadcast_ssid=0
wpa=2
wpa_passphrase=AardvarkBadgerHedgehog
wpa_key_mgmt=WPA-PSK
wpa_pairwise=TKIP
rsn_pairwise=CCMP
```
edit additional hostpot config
```
sudo nano /etc/default/hostapd
```
find the line with #DAEMON_CONF, and replace it with this:
```
DAEMON_CONF="/etc/hostapd/hostapd.conf"
```
unmask and start
```
sudo systemctl unmask hostapd
sudo systemctl enable hostapd
sudo systemctl start hostapd

sudo systemctl start dnsmasq
```

## Software Install

### NodeJS

install native node support on raspberry

Either install ARM image from nodejs

[https://www.instructables.com/id/Install-Nodejs-and-Npm-on-Raspberry-Pi/](https://www.instructables.com/id/Install-Nodejs-and-Npm-on-Raspberry-Pi/)

Or use system install node and add/update npm

```
sudo apt-get -y install npm
sudo npm update
node -v
npm -v
```

### Docker on RPi

Install docker on PI
- [https://blog.hypriot.com/getting-started-with-docker-on-your-arm-device/](https://blog.hypriot.com/getting-started-with-docker-on-your-arm-device/)
- [https://www.raspberrypi.org/blog/docker-comes-to-raspberry-pi/](https://www.raspberrypi.org/blog/docker-comes-to-raspberry-pi/)
```
cd
mkdir docker
cd docker/
```
install latest
```
curl -sSL https://get.docker.com | sh
```
or install specific version (as of march 2020, latest validated for KB 1.10.8 => Docker 17.03.2)
```
export VERSION=18.03 && curl -sSL get.docker.com | sh
```
for Raspi4, current workaround is
- [https://forums.docker.com/t/release-for-raspberry-pi-4-raspbian-buster/77120/10](https://forums.docker.com/t/release-for-raspberry-pi-4-raspbian-buster/77120/10)
```
export VERSION=18.03 && curl -sSL get.docker.com | sed 's/buster/stretch/' | sh

sudo docker run hello-world
```
enable docker for user pi
```
sudo usermod -aG docker pi
```
**REQUIRED** to exit/log or reboot then test
```
sudo reboot
docker run hello-world
```
Docker issue with DNS nameserver nor working (no ping with hostname but IP ok)
- [https://development.robinwinslow.uk/2016/06/23/fix-docker-networking-dns/](https://development.robinwinslow.uk/2016/06/23/fix-docker-networking-dns/)

check that DNS not properly set for Docker
```
docker run busybox nslookup google.com
```
get DNS info (requires network-manager)
```
nmcli dev show | grep 'IP4.DNS'
```
update dns config for Docker and set Inria DNS
```
sudo vi /etc/docker/daemon.json
```
and add
```
{
    "dns": ["192.168.2.1","128.93.77.234"]
}

Freebox DNS: 192.168.0.254
{
    "dns": ["192.168.0.254"]
}
```
getting DNS server on host
```
cat /etc/resolv.conf
less /etc/resolv.conf
```
get IP addresses and interfaces, then get DNS server (Linux)
```
ip r | grep default
host -a www.cnn.com | grep from
dig www.yahoo.com | grep "SERVER"
```
ATTENTION restart docker daemon when changing DNS config
```
sudo service docker restart
```
ping with hostname
```
docker run busybox ping -c 10 www.cnn.com 
```

## Misc Topics

### SD Card dump

Dump only if really needed - (16Gb took 2300 sec - about 40min)

[https://thepihut.com/blogs/raspberry-pi-tutorials/17789160-backing-up-and-restoring-your-raspberry-pis-sd-card](https://thepihut.com/blogs/raspberry-pi-tutorials/17789160-backing-up-and-restoring-your-raspberry-pis-sd-card)

```
diskutil list
sudo dd if=/dev/disk2 of=~/SDCardBackup.dmg
diskutil unmountDisk /dev/disk2
```
