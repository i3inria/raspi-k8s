# Containers, Edge and Serverless

- Note 11/03/2020: This document is a WIP
- Note 08/08/2021: Major update to account for new tools/k8s distributions, Raspberry 4

# Introduction

This short document gives an overview of container orchestration, the ecosystem around Kubernetes, and some related projets on Edge Computing.
* For setting up a cluster of RPIs, check [RPI_CLUSTER](RPI_CLUSTER.md) and [NETWORKING](NETWORKING.md)
* For installing Kubernetes on a RPIs cluster, check [KUBERNETES](KUBERNETES.md)
* For setting up the Dashboard application in Kubernetes, check [DASHBOARD](DASHBOARD.md) as well as  [PODS](PODS.md)   for other apps (MQTT, Grafana, ...)
* For serverless on Kubernetes, check [OPENFASS](OPENFASS.md) 
* For a quick Ansible overview in the context of PIs cluster, check [ANSIBLE](../deprecated/ANSIBLE.md)

## Cloud and container orchestration.			

Today’s application release cycles have dramatically changed thanks to the explosive growth of cloud platforms, containers, and microservices-oriented architectures, the diversity of environments (e.g., Web/cloud, mobile, IoT), as well as evolutions in the development processes of applications (e.g., from packaging  applications into containers to automating their deployment and orchestration.

Central to these evolutions are containers. standalone, self-contained units that package software and its dependencies together. The most prominent container technology is currently Docker. Containers offer lightweight performance isolation, fast and flexible deployment, and fine-grained resource sharing. They are increasingly being used to deploy and scale applications on diverse infrastructures. While working with containers in the cloud is all about allocating resources effectively, things get complicated when using multiple clouds along with on-premises datacenter, and deploying a variety of applications on them. 

For such infrastructures, Container Orchestration Engines (COEs) provide an enterprise-level framework for integrating and managing containers at scale. COEs simplify container management and provide a framework not only to define initial container deployment but also to manage multiple containers as one entity -- for purposes of availability, scaling, and networking. COEs provide a range of services to both infrastructure managers as well as deployed applications. A good example would be service discovery, which is used to determine where various services are located on a network in a distributed computing architecture. Another good example is load balancing requests, whether internally generated in a cluster, or externally from outside clients.

COEs provide an abstraction layer between the application containers that run on Cloud resources and the actual resource pools themselves. Kubernetes, as well as other DevOps infrastructure management tools such as Docker Swarm, Mesos/Marathon, Nomad, Amazon ECS and many others fall into this class. 


### Main concepts of Kubernetes 

Kubernetes is a dynamic system that manages the deployment, management, and interconnection of containers on a fleet of worker servers. These worker servers where containers run are called Nodes, While Node components run on every node, maintaining running pods (sets of tightly-coupled containers) and providing the Kubernetes runtime environment, Master components provide the cluster’s control plane and make global decisions about the cluster (for example, scheduling), and detecting and responding to cluster events (starting up a new pod when a replication controller’s replicas field is unsatisfied).

The smallest deployable unit in a Kubernetes cluster is a Pod which typically consists of an application container and potentially “side” containers that perform some helper function like monitoring or logging. Containers in a Pod share storage resources, a network namespace, and port space. A Pod can be thought of as a group of containers that work together to provide a given feature. A complete application may be composed of multiple pods.


### Kubernetes details (from Kubernetes docs)

Pods are typically rolled out using Deployments, which are objects defined by YAML files that declare a particular desired state. For example, an application state could be running three replicas of the Express/Node.js web app container and exposing Pod port 8080. Once created, a controller on the Control Plane gradually brings the actual state of the cluster to match the desired state declared in the Deployment by scheduling containers onto Nodes as required. Using Deployments, a service owner can easily scale a set of Pod replicas horizontally or perform a zero-down- time rolling update to a new container image version by simply editing a YAML file and performing an API call (e.g. by using the command line client kubectl). Deployments can quickly be rolled back, paused, and resumed.

Once deployed, a Service can be created to allow groups of similar deployed Pods to receive traffic (Services can also be created simultaneously with Deployments). Services are used to grant a set of Pod replicas a static IP and configure load balancing between them using either cloud provider load balancers or user-specified custom load balancers. Services also allow users to leverage cloud provider firewalls to lock down external access.

To start and manage Pods and their containers on worker machines, Nodes run an agent process called kubelet which communicates with a kube-apiserver on the Control Plane. Using a container runtime like Docker, also running on Nodes, these scheduled containers are first pulled as images from either a private or public image registry, and then created and launched. Nodes also run a kube-proxy, which manages network rules on the host.

The Kubernetes Control Plane oversees the Nodes and manages their scheduling and maintains their workloads. It consists of the kube-apiserver front-end, backed by the key-value store etcd to store all the cluster data. Finally, a kube-scheduler schedules Pods to Nodes, and a set of controllers continuously observe the state of the cluster and drive its actual state towards the desired state.


### References

[1] Docker. [https://www.docker.com](https://www.docker.com).

[2] Kubernetes. [https://kubernetes.io](https://kubernetes.io)

[3] Marathon. [https://mesosphere.github.io/marathon](https://mesosphere.github.io/marathon).

[4] Nomad. [https://www.nomadproject.io/](https://www.nomadproject.io/)


## Ecosystem Overview


### Organisations

**CNCF** (Cloud Native Computing Foundation) [https://www.cncf.io/](https://www.cncf.io/) 

CNCF is an open source software foundation dedicated to making cloud native computing universal and sustainable. Neutral home for many of the fastest-growing projects on GitHub, including Kubernetes, Prometheus and Envoy, fostering collaboration between the industry’s top developers, end users, and vendors.


### Software components

Kubernetes is just one technology of the CNCF ecosystem. It's itself leveraging many other technologies for networking, databases, ...

Check the [Interactive map](https://landscape.cncf.io/) of these technologies.


## Edge Computing

### Cluster Federation

K8S Working Group: [https://docs.google.com/document/d/1v-Kb1pUs3ww_x0MiKtgcyTXCAuZlbVlz4_A9wS3_HXY/](https://docs.google.com/document/d/1v-Kb1pUs3ww_x0MiKtgcyTXCAuZlbVlz4_A9wS3_HXY/)

Edge projects

https://dzone.com/articles/open-source-iot-edge-projects


### Kubeedge

Website: [https://kubeedge.io/en/](https://kubeedge.io/en/)

Documentation: [https://docs.kubeedge.io/en/latest/](https://docs.kubeedge.io/en/latest/)

Repository: [https://github.com/kubeedge](https://github.com/kubeedge)

[https://kubeedge.io/en/#why-kubeedge](https://kubeedge.io/en/#why-kubeedge)




