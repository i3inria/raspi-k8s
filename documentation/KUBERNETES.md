# Kubernetes

## General

All the [concepts](https://kubernetes.io/docs/concepts/)

References

Websites

People

[https://medium.com/@betz.mark](https://medium.com/@betz.mark)


### Networking concepts

- Kubernetes’s **Service** is the persistent abstraction of an application running on a set of Pods (which are transient). More on services [here](https://kubernetes.io/docs/concepts/services-networking/service/)
- **ClusterIP**: Exposes the service on a cluster-internal IP. Choosing this value makes the service only reachable from within the cluster. This is the default ServiceType
- **NodePort**: Exposes the service on each Node’s IP at a static port (the NodePort). A ClusterIP service, to which the NodePort service will route, is automatically created. You’ll be able to contact the NodePort service, from outside the cluster, by requesting &lt;NodeIP>:&lt;NodePort>.
- **LoadBalancer**: Exposes the service externally using a cloud provider’s load balancer. NodePort and ClusterIP services, to which the external load balancer will route, are automatically created. Requires a LoadBalancing controller component (not in core k8s).

A **[discussion](https://stackoverflow.com/questions/41509439/whats-the-difference-between-clusterip-nodeport-and-loadbalancer-service-types)** between the 3, and then [discussion](https://www.ovh.com/blog/getting-external-traffic-into-kubernetes-clusterip-nodeport-loadbalancer-and-ingress/) with Ingress also, as well as this [discussion](https://medium.com/google-cloud/kubernetes-nodeport-vs-loadbalancer-vs-ingress-when-should-i-use-what-922f010849e0).

A very goos series of posting on  **networking/routing** in Docker/Kubernetes with pods ([part 1](https://medium.com/google-cloud/understanding-kubernetes-networking-pods-7117dd28727))  services ([part 2](https://medium.com/google-cloud/understanding-kubernetes-networking-ingress-1bc341c84078)) and ingress ([part 3)](https://medium.com/google-cloud/understanding-kubernetes-networking-ingress-1bc341c84078) Images below are taken from these posts.

In plain Docker, each container has its own virtual interface and is connected to the Docker0 bridge that connects the containers’ network and the node’s network.


<p id="gdcalert1" ><span style="color: red; font-weight: bold">>>>>>  gd2md-html alert: inline image link here (to images/Edge-and0.png). Store image on your image server and adjust path/filename if necessary. </span><br>(<a href="#">Back to top</a>)(<a href="#gdcalert2">Next alert</a>)<br><span style="color: red; font-weight: bold">>>>>> </span></p>


![alt_text](images/Edge-and0.png "image_tooltip")


In Kubernetes, each Pod is a set of containers (1 or more) sharing a virtual interface/address so they (the containers within the Pod) can communicate thru localhost and thus share the ports. Each Pod is then connected to the k8s bridge of the host. The bridge interfaces the actual host’s network interface and a private network for the pods to interact. Routing between hosts/pods is managed by k8s.  K8s ensures that all Pods in the cluster, even in different hosts, have different IP addresses and that routing across Pods can be achieved.

<p id="gdcalert2" ><span style="color: red; font-weight: bold">>>>>>  gd2md-html alert: inline image link here (to images/Edge-and1.png). Store image on your image server and adjust path/filename if necessary. </span><br>(<a href="#">Back to top</a>)(<a href="#gdcalert3">Next alert</a>)<br><span style="color: red; font-weight: bold">>>>>> </span></p>

![alt_text](images/Edge-and1.png "image_tooltip")

Because Pods are not durable, their IP address cannot be used as a reliable endpoint. Thus they need a reverse-proxy/load balancer that has a durable address and that can dynamically redirect the traffic to the active Pod.

A Service object can be deployed in order to offer a durable endpoint for Pods. A Service is deployed on a different virtual private network (the Service network) than the Pods or the hosts. However, is should be noted that the service network does not exist in the system (i.e., interfaces or routing rules). The service network is managed by the kube-proxy component (in user space), that leverages netfilter (in kernel space) to redirect dynamically the packets to the proper host/node using its rules-based packet processing engine. 

When a service  (of default type Cluster IP) is created/terminated, the kube-proxy component then uses the iptables interface to write the rules in netfilter. Health checks will also trigger kube-proxy, and thus netfilter rules updates if a host/Pod gets down.

### Service types

There are 3 types of services: Cluster IP (default), NodePort, and LoadBalancer. 

A service of type NodePort is a ClusterIP service with an additional capability: it is reachable at the IP address of the node as well as at the assigned cluster IP on the services network.

*   As with ClusterIP services, the service has a port on the service network (the Port)
*   Additionally, kube-proxy assigns a port in the range 30000–32767 on the eth0 interface (the NodePort)
*   All traffic to the eth0:NodePort is forwarded to ClusterIP:Port
*   While it exposes services on a non-standard port, a classic reverse-proxy/load balancer (nginx) on the host can do the url path redirect, or expose the normal port and redirect to the NodePort for the external clients.

<p id="gdcalert3" ><span style="color: red; font-weight: bold">>>>>>  gd2md-html alert: inline image link here (to images/Edge-and2.png). Store image on your image server and adjust path/filename if necessary. </span><br>(<a href="#">Back to top</a>)(<a href="#gdcalert4">Next alert</a>)<br><span style="color: red; font-weight: bold">>>>>> </span></p>


![alt_text](images/Edge-and2.png "image_tooltip")


Additionally, Ingress resources allow (among many other things) to route packets to Pods. 

ClusterIP vs NodePort vs Ingress [article](https://medium.com/google-cloud/kubernetes-nodeport-vs-loadbalancer-vs-ingress-when-should-i-use-what-922f010849e0)

Expose services outside cluster [article](https://gardener.cloud/050-tutorials/content/howto/service-access/)

Exposing an External IP Address to Access an Application in a Cluster [DOC REF k8s](https://kubernetes.io/docs/tutorials/stateless-application/expose-external-ip-address/)

Deploying PHP Guestbook application [[DOC k8s](https://kubernetes.io/docs/tutorials/stateless-application/guestbook/#scale-the-web-frontend) scale service up/down example]

Relying only on basic kubernetes 

pimaster1.local:1880/

Guy installing an Ingress controller on raspi cluster [https://github.com/geerlingguy/raspberry-pi-dramble/issues/103](https://github.com/geerlingguy/raspberry-pi-dramble/issues/103)

### Debugging

In development mode, if you want to access a container running internally, you can use kubectl port-forward:

On the master node

```
kubectl port-forward <container-pod> 1880:9090
```


Then, http://localhost:9090 will show the container-pod screen because you have kubectl access (kubectl port-forward may not work for services yet) 

how to edit a resource at runtime and automatically push changes
```
kubectl edit -n kube-system deploy fluentd 

kubectl describe svc
```
enter shell on a Pod’s container
```
kubectl get pods
kubectl exec -it <POD_NAME> /bin/sh
```




<p id="gdcalert4" ><span style="color: red; font-weight: bold">>>>>>  gd2md-html alert: inline image link here (to images/Edge-and3.png). Store image on your image server and adjust path/filename if necessary. </span><br>(<a href="#">Back to top</a>)(<a href="#gdcalert5">Next alert</a>)<br><span style="color: red; font-weight: bold">>>>>> </span></p>


![alt_text](images/Edge-and3.png "image_tooltip")


*   LoadBalancer (services) is all about extending a single service to support external clients.
    *   NodePort service plus the ability to build out a complete ingress path
*   Ingress (resources) is about much more, For example TLS termination, virtual hosts, and path-based routing. Ingress handles load balancing for multiple backend services.
    *   services must be of type NodePort

While kube-proxy is part of the core kubernetes infrastructure, LoadBalancer and Ingress require additional components, usually part of the platform providing kubernetes (e.g., GCP, AWS). For bare kubernetes

Warning: mixing LoadBalancer and Ingress in a cluster can introduces routing issues. Best to only use one kind.


## Accessing application’s services


### General access policies/methods

**StatefulSet and accessing individual instances directly**: [Article](https://itnext.io/exposing-statefulsets-in-kubernetes-698730fb92a1) on how to use headless services’ endpoints to generate instance-specific DNS records in the form of: &lt;StatefulSet>-&lt;Ordinal>.&lt;Service>.&lt;Namespace>.svc.cluster.local, and also accessing individual replicas in a StatefulSet using NodePort with attribute externalTrafficPolicy to Local (prevents forwarding to another node).

### Example accessing service with Ingress

Need first to launch some test application with services

*   A [simple](https://medium.com/google-cloud/understanding-kubernetes-networking-services-f0cb48e4cc82) python image program displaying the hostname with 2 replicas
*   A [nodejs](https://kubernetes.io/docs/tutorials/stateless-application/expose-external-ip-address/) hello world server with 5 replicas (not for arm/RPi)

On the RPi, use the simple python image (above) and follow the tutorial to

* Deploy the 2 replicas test Pod
* Deploy the ClusterIP and NodePort services for this Pod
* Tst clients Pods and access the services

To test the services

*   On the master
    *   get the service pods’ IP addreses (10.244.1.253/354)
    *   `kubectl get pods -o wide`
    *   get the service Cluster IP and port  (10.104.40.217 and 80)
    *   `kubectl get svc -o wide`
*   Log on one of the service Pod’s container, 
    *   ping both pods IP addresses
    *   `ping 10.244.1.253`
    *   then use curl to retrieve the service’s output using curl (should alternate between the pods)
    *   `curl  10.104.40.217:80`
*   On the master
    *   curl the service (should alternate between the pods)
    *   `curl  10.104.40.217:80`
*   After the NodePort service is deployed
    *   curl using the master’s IP address and NodePort
    *   `curl  192.168.0.55:32638 `


## Setting up Ingress

Check above for networking, load balancing, Ingress.

General Ingress information

Nginx Ingress controllers:


*   From Kubernetes [website](https://kubernetes.github.io/ingress-nginx/) and [github](https://github.com/kubernetes/ingress-nginx)
*   From Nginx [website](https://www.nginx.com/products/nginx/kubernetes-ingress-controller/) and [github](https://github.com/nginxinc/kubernetes-ingress)

Bare metal kubernetes Nginx install

[https://github.com/kubernetes/ingress-nginx/blob/master/docs/deploy/index.md#bare-metal](https://github.com/kubernetes/ingress-nginx/blob/master/docs/deploy/index.md#bare-metal)

Bare metal

[https://github.com/kubernetes/ingress-nginx/blob/master/docs/deploy/baremetal.md](https://github.com/kubernetes/ingress-nginx/blob/master/docs/deploy/baremetal.md)

Ingress on Raspi Cluster (using Kubernetes’s nginx-Ingress)

Deployment of the nginx Ingres controller: follow the [install doc](https://kubernetes.github.io/ingress-nginx/) but in the deployment file, replace  `nginx-ingress-controller` with `nginx-ingress-controller-arm` inthe image declaration.



*   To [update](https://kubernetes.github.io/ingress-nginx/deploy/upgrade/) the deployment of the Ingress controller, simply change the version number.either assignment or interactively.

assign/patch External-IP address to a service
```
kubectl patch svc svc_name -n namespace -p '{"spec":{"externalIPs":["ex_ip"]}}'
```
for cluster B
```
kubectl patch svc ingress-nginx -n ingress-nginx   -p '{"spec": {"externalIPs":["192.168.0.55"]}}'
```
nginx service description
```
kubectl describe svc ingress-nginx -n ingress-nginx
```
output:
```
Name:                 	ingress-nginx
Namespace:            	ingress-nginx
Labels:               	app.kubernetes.io/name=ingress-nginx
                      	app.kubernetes.io/part-of=ingress-nginx
Annotations:          	kubectl.kubernetes.io/last-applied-configuration={"apiVersion":"v1","kind":"Service","metadata":{"annotations":{},"labels":{"app.kubernetes.io/name":"ingress-nginx","app.kubernetes.io/part-of":"ingres...
Selector:             	app.kubernetes.io/name=ingress-nginx,app.kubernetes.io/part-of=ingress-nginx
Type:                 	NodePort
IP:                   	10.100.182.144
External IPs:         	192.168.0.55
Port:                 	http  80/TCP
TargetPort:           	80/TCP
NodePort:             	http  30721/TCP
Endpoints:            	10.244.1.247:80
Port:                 	https  443/TCP
TargetPort:           	443/TCP
NodePort:             	https  31085/TCP
Endpoints:            	10.244.1.247:443
Session Affinity:     	None
External Traffic Policy:  Cluster
Events:               	<none>
```
get nginx ingress controller configuration (!! nginx logs redirected to std out/err > kubectl logs)
```
kubectl exec -it -n <namespace-of-ingress-controller> nginx-ingress-controller-XXXXX cat /etc/nginx/nginx.conf
```
get/delete Ingress rules
```
kubectl get ingress
kubectl describe ingress service-test
kubectl delete ingress service-test
```
setup the ConfigMap for the service: `vi configmap-test-service.yaml`
```
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  name: service-test
  namespace: default
  annotations:
	# use the shared ingress-nginx
	kubernetes.io/ingress.class: "nginx"
spec:
  rules:
  - host: "test.pimaster.lan"
	http:
  	paths:
  	- path: /toto
    	backend:
      	serviceName: service-test
      	servicePort: 80
```

and apply
```
kubectl apply -f configmap-test-service.yaml
```
then on remote node that defines `test.pimaster.lan` in the `/etc/hosts`
```
curl test.pimaster.lan
```
if want to [redirect](https://kubernetes.github.io/ingress-nginx/examples/rewrite/) based on path (e.g., /foo for serviceX or /bar for serviceY) need to add path, and also add rewrite annotation and rule in path to remove foo/bar from path
```
 annotations:
    nginx.ingress.kubernetes.io/rewrite-target: /$2
```
And
```
    	backend:
    serviceName: service-test
      	servicePort: 80
    	path: /foo(/|$)(.*)
```
then 
```
curl test.pimaster.lan/foo
```


However, [problem](https://stackoverflow.com/questions/55630746/kubernetes-ingress-configuration-rewrite-problem) with the URL path removed if additional files with relative path are downloaded. Need to return the full public URL. 

Ingress troubleshooting [guide](https://kubernetes.github.io/ingress-nginx/troubleshooting/).

**NOTE**: alternatively from NodePort (or in addition, since NodePort/HostPort are not exclusive) can use HostPort to map directly the host/node network interface for the Pod.

[https://github.com/kubernetes/ingress-nginx/blob/master/docs/deploy/baremetal.md#via-the-host-network](https://github.com/kubernetes/ingress-nginx/blob/master/docs/deploy/baremetal.md#via-the-host-network)

Maybe ssl config for ingress [???](https://daemonza.github.io/2017/02/13/kubernetes-nginx-ingress-controller/)

get again the target service’s description. 
```
kubectl describe services service-test

Name:                   service-test
Namespace:              default
Labels:                 <none>
Selector:               app=service_test_pod
Type:                   ClusterIP
IP:                     10.3.241.152
Port:                   http    80/TCP
Endpoints:              10.0.1.2:8080,10.0.2.2:8080
Session Affinity:       None
Events:                 <none>
```


Pods

Services



*   Accessing services outside the cluster


## Commands

Commands cheatsheet: 


- https://kubernetes.io/fr/docs/reference/kubectl/cheatsheet/


*   [https://kubernetes.io/docs/reference/kubectl/cheatsheet/](https://kubernetes.io/docs/reference/kubectl/cheatsheet/)

**# Cluster reset**
```
sudo kubeadm reset
```
This command reverts any changes made by kubeadm init or kubeadm join.

[https://kubernetes.io/docs/reference/setup-tools/kubeadm/kubeadm-reset/](https://kubernetes.io/docs/reference/setup-tools/kubeadm/kubeadm-reset/)

**# List pods/deployments** (that restarts pods)
```
kubectl get pods --all-namespaces
kubectl get pods -a -o wide --all-namespaces
kubectl get deployments --all-namespaces
```
**# Delete a pod** (do not forget namespace)
```
kubectl delete pod kubernetes-dashboard-869db7f6b4-ddlmq -n kube-system
```
-> If not working (pod restarted), need to delete deployment
```
kubectl get deployments --all-namespaces
kubectl delete -n NAMESPACE deployment DEPLOYMENT
```
may need to check also jobs and replicasets
```
kubectl get rs --all-namespaces
```
also may need to terminate stuck pod
```
kubectl delete pods podname --grace-period=0 --force
```
**# describe pod config **
```
kubectl describe pod --namespace=kube-system
```
**Proxy cluster** to master node’s host. (any port can be selected - default is 8001)
```
kubectl proxy
kubectl proxy --port=8080
```
get events 
```
kubectl get events

sudo iptables -L
tail -f /var/log/syslog
```

**How to restart a pod ? : scale it down/up**

scale down/terminate instance
```
kubectl scale deployment kubernetes-dashboard-7df4f4cc8b-7bnvg --replicas=0 -n kube-system
```
check terminated
```
kubectl get deployments --all-namespaces
```
scale up, use pod name
```
kubectl scale deployment kubernetes-dashboard --replicas=1 -n kube-system
```
check active
```
kubectl get deployments --all-namespaces
```
**On the master**

get pods
```
kubectl get pods -o wide --all-namespaces
```
deploy an application (service and deployment)

get all K8S info
```
kubectl get all
```
delete a service and deployment
```
kubectl delete service/nodered deployment.apps/nodered
```

### deploy nodered
```
cd /home/pi/git/raspi-k8s/k8s/iot_test_app
kubectl  apply -n default  -f depl_nodered.yml
```


// get services, detailed info for a service, and its endpoint

    kubectl get svc
    kubectl describe svc nodered
    kubectl get ep nodered


## Error messages

sudo kubctl commands triggers `"The connection to the server localhost:8080 was refused - did you specify the right host or port?"`

*   For `kubectl`, once install finished and init config in home, do not use `sudo` but directly `kubertl` as the user, otherwise error get this message 

kubctl commands trigger  `kubectl certificate signed by unknown authority`.

*   If doing a `reset` and doing a `kubectl init` again, do not forget to redo K8S config copy in home 


```
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config
```


Or (if no home dir .kube config folder) as root do


```
export KUBECONFIG=/etc/kubernetes/admin.conf
```


Certificates issues when reinstalling a pod. 



*   The deletion of a pod/deployment may leave some config file (certs). To properly remove everything, 


```
kubectl delete -f https://<recipe file>.yaml
```


Kubernetes error: Unable to connect to the server: dial tcp 127.0.0.1:8080

[https://stackoverflow.com/questions/51535654/kubectl-apply-command-does-not-work-gives-connection-refused-error](https://stackoverflow.com/questions/51535654/kubectl-apply-command-does-not-work-gives-connection-refused-error) 

Kubectl / CNI (e.g., Flannel) not properly setup, or HOME config not create/copied

A comparison (in french) of various networking solutions (https://www.objectif-libre.com/fr/blog/2018/07/05/comparatif-solutions-reseaux-kubernetes/)

### Changing the IP address of the K8S master

[https://stackoverflow.com/questions/52647072/my-kubernetes-cluster-ip-address-changed-and-now-kubectl-will-no-longer-connect](https://stackoverflow.com/questions/52647072/my-kubernetes-cluster-ip-address-changed-and-now-kubectl-will-no-longer-connect)






## Kubernetes installation

troubleshooting kubeadm, go here [https://kubernetes.io/docs/setup/independent/troubleshooting-kubeadm/](https://kubernetes.io/docs/setup/independent/troubleshooting-kubeadm/)

Install kubernetes on RPi

**Older instructions (2015) - not working anymore**

[https://kubernetes.io/blog/2015/12/creating-raspberry-pi-cluster-running/](https://kubernetes.io/blog/2015/12/creating-raspberry-pi-cluster-running/)

**Instead, use recent documentation (2018) from actual RPi clusters**

[https://blog.sicara.com/build-own-cloud-kubernetes-raspberry-pi-9e5a98741b49](https://blog.sicara.com/build-own-cloud-kubernetes-raspberry-pi-9e5a98741b49)

[https://gist.github.com/alexellis/fdbc90de7691a1b9edb545c17da2d975](https://gist.github.com/alexellis/fdbc90de7691a1b9edb545c17da2d975)

Check swap
```
sudo swapon --summary
```
Disable swap
```
sudo dphys-swapfile swapoff && \
sudo dphys-swapfile uninstall && \
sudo update-rc.d dphys-swapfile remove
```
check disabled
```
sudo swapon --summary
```
After reboot, should not be back on. If problem (Raspbian buster) then check 

[https://www.raspberrypi.org/forums/viewtopic.php?t=244130](https://www.raspberrypi.org/forums/viewtopic.php?t=244130)

```
vi /etc/dphys-swapfile
```
And edit for 
```
CONF_SWAPSIZE=0
```

**Issue with master not being abole to ping** service endpoint IP address on the Raspi4, but other nodes are fine. Related to  IPTables support - Buster Issue - IPTable >= 1.8 not [supported](https://github.com/geerlingguy/raspberry-pi-dramble/issues/156), 

**[workaround](https://github.com/geerlingguy/raspberry-pi-dramble/issues/156)**

    sudo update-alternatives --set iptables /usr/sbin/iptables-legacy

copy at the end of the line
```
sudo vi /boot/cmdline.txt
```
content to copy
```
cgroup_enable=cpuset cgroup_memory=1 cgroup_enable=memory
```
then restart
```
sudo reboot
```
**# Installing a specific version of Kubernetes**

If installing Dashboard, need to check compatibility with k8s version and install the proper version [https://github.com/kubernetes/dashboard/releases](https://github.com/kubernetes/dashboard/releases)

custom install

[https://stackoverflow.com/questions/49721708/how-to-install-specific-version-of-kubernetes](https://stackoverflow.com/questions/49721708/how-to-install-specific-version-of-kubernetes)

**install kubadm (latest)**
```
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add - && \
  echo "deb http://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list && \
  sudo apt-get update -q && \
  sudo apt-get install -qy kubeadm
```

**Install 1.10.8**

need to install kubelet/kubectl/kubeadm with the same version

getting issue  kubelet : Depends: kubernetes-cni (= 0.6.0) but 0.7.5-00 is to be installed

[https://github.com/kubernetes/kubernetes/issues/75683](https://github.com/kubernetes/kubernetes/issues/75683)

Need to install cni before
```
sudo apt-get install kubernetes-cni=0.6.0-00
```
then redo kubeXXX=1.10.8-00 again…
```
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add - && \
  echo "deb http://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list && \
  sudo apt-get update -q && \
  sudo apt-get install -qy kubelet=1.10.8-00 kubectl=1.10.8-00 kubeadm=1.10.8-00
```

## Networking setup

pull images (not necessary ?)
```
sudo kubeadm config images pull -v3
```
### Weave


### Flannel

By default, Kubernetes uses weave. Specifying the flag --pod-network-cidr to kubeadm forces Flannel.

Depending on the network add-on (flannel, …),a pod-network-cidr must me defined - check  [https://kubernetes.io/fr/docs/setup/independent/create-cluster-kubeadm/](https://kubernetes.io/fr/docs/setup/independent/create-cluster-kubeadm/)

using Flannel (change the last IP to the one of the master/current node)
```
sudo kubeadm init --token-ttl=0 --pod-network-cidr=10.244.0.0/16 --apiserver-advertise-address=192.168.2.19
```
if getting Docker version “ERROR”  (Pb with matching versions of Docker and K8S

[https://stackoverflow.com/questions/53256739/which-kubernetes-version-is-supported-in-docker-version-18-09](https://stackoverflow.com/questions/53256739/which-kubernetes-version-is-supported-in-docker-version-18-09)

Add `--ignore-preflight-errors=SystemVerification` to kubeadm init command above.

as mentioned afterwards
```
mkdir -p $HOME/.kube
rm $HOME/.kube/config
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config
```
**ATTENTION : copy/paste the command with the token for later….**

Your Kubernetes master has initialized successfully!

To start using your cluster, you need to run the following as a regular user:

    mkdir -p $HOME/.kube
    sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
    sudo chown $(id -u):$(id -g) $HOME/.kube/config

You should now deploy a pod network to the cluster.
Run "kubectl apply -f [podnetwork].yaml" with one of the options listed at:
  https://kubernetes.io/docs/concepts/cluster-administration/addons/

You can now join any number of machines by running the following on each node
as root:

    kubeadm join 192.168.0.55:6443 --token qrcegc.yregdhi3nnd5wctc --discovery-token-ca-cert-hash sha256:98049530c949d2abe1502eedfd38a48b6eaa5ba5463142fee5e253b079cd346c

    # reboot
    sudo reboot

    # check kube daemon restarted - may take a few minutes to be up and running
    kubectl get pods -o wide --all-namespaces

    systemctl status kubelet
    systemctl daemon-reload


**# config checks**

check that `/run/flannel/subnet.env` file exists
```
more /run/flannel/subnet.env
```
deploy a pod network to the cluster - here flannel (did set cidr fla above during init)

most recent ?
```
kubectl -n kube-system apply -f https://raw.githubusercontent.com/coreos/flannel/bc79dd1505b0c8681ece4de4c0d86c5cd2643275/Documentation/kube-flannel.yml
```
after 2-3 minutes, flannel pod should be up and running, and env file created
```
kubectl get pods -o wide --all-namespaces
more /run/flannel/subnet.env
```
If error during install “<code>unable to recognize "[https://raw.githubusercontent.com](https://raw.githubusercontent.com) …</code>” Then issue with kubeadm + setup/copy in $HOME folder above.
```
sudo sysctl net.bridge.bridge-nf-call-iptables=1
```
list the pods and status
```
kubectl get pods -o wide --all-namespaces
```
**ERROR with pod Dashboard** : CrashLoopBackOff

get the details on the failing pod (do not forget namespace)
```
kubectl describe pod kubernetes-dashboard-669f9bbd46-mvs99 -n kube-system
```

**ERROR with pod kube-flannel-ds-arm-lkgmx** : CrashLoopBackOff
```
kube-system   kube-flannel-ds-arm-lkgmx  0/1 Error 	...
```
Or 
```
kube-system   kube-dns-686d6fb9c-tlr9d  0/3 ContainerCreating   ...
kube-system   kube-flannel-ds-arm-lkgmx 0/1 CrashLoopBackOff	….
```
Steps to solve

- check log (change pod name)
```
kubectl logs --namespace kube-system kube-flannel-ds-arm-lkgmx  -c kube-flannel --follow=true
```
- check IP addresses - on the host

Issue [https://github.com/coreos/flannel/issues/883](https://github.com/coreos/flannel/issues/883) 
```
ip addr
```
Returns
```
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
	link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
	inet 127.0.0.1/8 scope host lo
   	valid_lft forever preferred_lft forever
	inet6 ::1/128 scope host
   	valid_lft forever preferred_lft forever
2: eth0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc pfifo_fast state DOWN group default qlen 1000
	link/ether b8:27:eb:9d:66:a3 brd ff:ff:ff:ff:ff:ff
3: wlan0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
	link/ether 12:22:b9:82:f9:00 brd ff:ff:ff:ff:ff:ff
	inet 192.168.2.50/24 brd 192.168.2.255 scope global wlan0
   	valid_lft forever preferred_lft forever
	inet6 fe80::c773:901e:4cc3:e09e/64 scope link
   	valid_lft forever preferred_lft forever
4: docker0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc noqueue state DOWN group default
	link/ether 02:42:63:30:2e:4a brd ff:ff:ff:ff:ff:ff
	inet 172.17.0.1/16 brd 172.17.255.255 scope global docker0
   	valid_lft forever preferred_lft forever
	inet6 fe80::42:63ff:fe30:2e4a/64 scope link
   	valid_lft forever preferred_lft forever
5: flannel.1: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1450 qdisc noqueue state UNKNOWN group default
	link/ether e2:90:bb:15:c4:1a brd ff:ff:ff:ff:ff:ff
	inet 10.244.0.0/32 scope global flannel.1
   	valid_lft forever preferred_lft forever
	inet 169.254.91.84/16 brd 169.254.255.255 scope global flannel.1
   	valid_lft forever preferred_lft forever
6: cni0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1450 qdisc noqueue state UP group default qlen 1000
	link/ether 0a:58:0a:f4:00:01 brd ff:ff:ff:ff:ff:ff
	inet 10.244.0.1/24 scope global cni0
   	valid_lft forever preferred_lft forever
	inet 169.254.144.111/16 brd 169.254.255.255 scope global cni0
   	valid_lft forever preferred_lft forever
	inet6 fe80::7803:bcff:fe8b:4ee4/64 scope link
   	valid_lft forever preferred_lft forever
15: veth04cb16a8@if3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1450 qdisc noqueue master cni0 state UP group default
	link/ether ca:fc:85:6d:f3:63 brd ff:ff:ff:ff:ff:ff link-netnsid 0
	inet 169.254.193.9/16 brd 169.254.255.255 scope global veth04cb16a8
   	valid_lft forever preferred_lft forever
	inet6 fe80::c8fc:85ff:fe6d:f363/64 scope link
   	valid_lft forever preferred_lft forever
```
**Solution** : delete flannel.1 interface, will be recreated properly
```
sudo ip link delete flannel.1
```
then check restarting properly (2-3 minutes on RasPi)
```
kubectl get pods -o wide --all-namespaces
ip addr
kubectl describe pod  -n kube-system kube-flannel-ds-arm-lkgmx
kubectl logs --namespace kube-system kube-flannel-ds-arm-lkgmx  -c kube-flannel --follow=true
```


## RPi Kubernetes cluster
*   [https://kubernetes.io/blog/2015/12/creating-raspberry-pi-cluster-running/](https://kubernetes.io/blog/2015/12/creating-raspberry-pi-cluster-running/)
*   [https://blog.hypriot.com/post/setup-kubernetes-raspberry-pi-cluster/https://blog.hypriot.com/post/setup-kubernetes-raspberry-pi-cluster/](https://blog.hypriot.com/post/setup-kubernetes-raspberry-pi-cluster/https://blog.hypriot.com/post/setup-kubernetes-raspberry-pi-cluster/)

kubectl config view

```
http://localhost:8001/ui

https://kubernetes.io/docs/setup/independent/create-cluster-kubeadm/#pod-network

  134  cd kubernetes/
  135  curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add - &&   echo "deb http://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list &&   sudo apt-get update -q &&   sudo apt-get install -qy kubeadm
  136  dmesg
  137  $ sudo kubeadm config images pull -v3
  138  sudo kubeadm config images pull -v3
  139  sudo kubeadm init --token-ttl=0 --pod-network-cidr=10.244.0.0/16
  140  kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v1.10.1/src/deploy/recommended/kubernetes-dashboard.yaml
  141  kubectl proxy
  142  kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/c5d10c8/Documentation/kube-flannel.yml
  143  kubectl proxy

Your Kubernetes master has initialized successfully!

To start using your cluster, you need to run the following as a regular user:

  mkdir -p $HOME/.kube
  sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
  sudo chown $(id -u):$(id -g) $HOME/.kube/config

You should now deploy a pod network to the cluster.
Run "kubectl apply -f [podnetwork].yaml" with one of the options listed at:
  https://kubernetes.io/docs/concepts/cluster-administration/addons/

You can now join any number of machines by running the following on each node
as root:

  kubeadm join 192.168.2.19:6443 --token fiji8l.wq3doh3xnxu1k4t1 --discovery-token-ca-cert-hash sha256:fa4268fe04cce7f2f5aae584e8d32d6ee32976411fc9dfbbf439a1fc2047904d

  141  mkdir -p $HOME/.kube
  142  sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
  143  sudo chown $(id -u):$(id -g) $HOME/.kube/config
  144  kubectl get pods --all-namespaces
  145  kubectl get events --namespace=kube-system
```

### IBM Serverless

Installing WiringPI in order to use GPIO [http://wiringpi.com/](http://wiringpi.com/)
```
git clone git://git.drogon.net/wiringPi
cd wiringPi/
git pull origin master
./build 
gpio readall
cd ..

Installing
git clone git://github.com/ninjablocks/433Utils.git
cd 433Utils/RPi_utils
make all
cd ../..

git clone github.com/IBM/serverless-home-automation/opt/
   24  git clone github.com/IBM/serverless-home-automation
   25  node
   33  ls
   34  cd ..
```

#### installing serveless - CLI tool to deploy on multiple cloud platforms

[https://serverless.com/framework/docs/](https://serverless.com/framework/docs/)


```
sudo npm install -g serverless
serverless --version
```

Download nodejs node from OpenWhisk and uncompress in openwisk folder (openwhisk-runtime-nodejs-1.12.0-incubating-sources.tar.gz) 
```
cd
mkdir openwhisk
cd openwhisk/
cd incubator-openwhisk-runtime-nodejs-1.12.0-incubating/
   49  ls
   50  more README.md
```


