# Automated k8s install with Ansible


## Pre-requisites

We assume that you have a cluster of Raspberry Pi available (3 RPi at least) and Ansible installed on your computer (check the Ansible [introduction](https://sed-paris.gitlabpages.inria.fr/developer-meetups/2019-05-28/ansible_devmeetup.pdf) and [install](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html#from-pip)).

The RPis may be connected using:
- the WiFi or the Ethernet interface, connected to a router that provides internet access. The RPis have static 
IP addresses. The Ansible master is also on this network.
- the Wifi (alternatively Ethernet) interface for Internet access and k8s network overlay deployment, while the ethernet (alternatively WiFi) interface is used for 
collecting monitoring/logs or managing the nodes (e.g., when experiments involve altering the connectivity between 
the k8s nodes.

## Install procedure

First prepare the sdcard according to **0_sdcard** so that headless system/OS install and basic configuration is done.

Then launch the playbook in **1_system** to update the OS and install basic packages such as Python  (system_1.sh), 
install Docker (system_2.sh), and then finalize Docker configuration after a mandatory reboot (system_3.sh). 
This subfolder also contains maintenance scripts to reconfigure the network interfaces if the cluster is moved to 
another environment (e.g., home and work).

Then **2_k3s** contains the install and initialisation playbooks for kubernetes, as well as reset and uninstall 
maintenance playbooks. The playbook also installs the Kubernetes Dashboard.

Getting the token for the dashboard

    sudo kubectl -n kubernetes-dashboard describe secret $(sudo kubectl -n kubernetes-dashboard get secret | grep admin-user-token | awk '{print $1}')

The **3_pods** folder contains playbooks to install various applications on top of Kubernetes. Currently, a 
playbook to deploy OpenFaaS is provided.
 
The **4_docker_registry** folder contains playbook and scripts to install a secure private registry within the cluster.

The **5_docker_images** folder contains playbook to deploy, build and store in the local registry some Docker images. 
The current playbook deploys the Mosquitto server for Raspberry-stretch.

The **maintenance** folder contains playbooks to perform various operations on the cluster.

The **setups** folder contains the inventory files for the RPi clusters being deployed (public/private keys are 
not commited). Currently 
- Cluster A with 1 master and 3 nodes, all RPi3
- Cluster B with 1 master (RPi3) and 2 nodes (1 RPi4 and one RPi3) 


## Ansible commands

### Simple commands

Check that current host can access the Raspi cluster's nodes

    ansible all --inventory-file=./setups/cluster_B/inv_cluster_b.yml -m ping

Get info about the Raspi cluster's nodes

    ansible all --inventory-file=./setups/cluster_B/inv_cluster_b.yml -m setup | less

### Playbooks

Limit to specific hosts

    ansible-playbook playbooks/PLAYBOOK_NAME.yml --limit "host1,host2"

Dry-run

    ansible-playbook playbooks/PLAYBOOK_NAME.yml --check

Set environment variable from the command line

    ansible all --inventory-file=./setups/cluster_B/inv_cluster_b.yml -m shell -a 'echo $TOTO' -e "TOTO=atmouser"
    ansible-playbook playbooks/atmo_playbook.yml -e "ATMOUSERNAME=atmouser"
    
    shell -a 'echo $TERM'
    
    ansible all --inventory-file=./setups/cluster_B/inv_cluster_b.yml -m shell -a 'echo $TOTO' -e "TOTO=atmouser"

