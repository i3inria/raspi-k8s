#!/bin/bash

# check swap
sudo swapon --summary

# disable swap
sudo dphys-swapfile swapoff && sudo dphys-swapfile uninstall && sudo update-rc.d dphys-swapfile remove

# check disabled
sudo swapon --summary

# add cgroup/memory at end of file if not already there
if [ -f "/boot/cmdline.txt" ]; then
  line=$(head -n 1 < "/boot/cmdline.txt")
  if [[ $line =~ " cgroup_enable=cpuset cgroup_memory=1 cgroup_enable=memory" ]]; then
   echo "It's there!"
  else
    # replace the first line
    newline="$line cgroup_enable=cpuset cgroup_memory=1 cgroup_enable=memory"
    sudo sed -i "1s/.*/$newline/" /boot/cmdline.txt
  fi
fi

# make sure swap desactivated on recent raspbian
sudo sed -i 's/^CONF_SWAPSIZE=.*/CONF_SWAPSIZE=0/' /etc/dphys-swapfile

# REQUIRED reboot
