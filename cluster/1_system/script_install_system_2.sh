#!/bin/bash

# in case of previous install (interactive overwrite in install shell blocks script)
sudo rm -f /usr/share/keyrings/docker-archive-keyring.gpg

# Buster workaround as docker currently not available
# will change nothin on stretch
# export VERSION=18.03 && curl -sSL get.docker.com | sed 's/buster/stretch/' | sh
curl -sSL get.docker.com | sudo sh

# testing
sudo docker run hello-world

# enable docker for user pi
sudo usermod -aG docker pi

sudo touch /etc/docker/daemon.json

# need to add public key (https://stackoverflow.com/questions/60137344/docker-how-to-solve-the-public-key-error-in-ubuntu-while-installing-docker)
# to check
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg

# install docker compose
sudo pip3 install docker-compose

# REQUIRED reboot

