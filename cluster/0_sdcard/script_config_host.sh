#!/bin/bash

#
# script to update the hostname on the initial raspberry node and copy ssh key
# done via laucnhing a script on the remote raspberry
#

if [ "$#" -ne 3 ]
then
  echo >&2 "usage: $0 new_rpi_hostname pub_key_file pi_user_password"
  echo >&2 "   - example: $0 pimaster1 ../setups/cluster_A/keys/cluster_a_rsa.pub new_password"
  exit 1
fi

# remove from know_hosts - hostname
ssh-keygen -R "raspberrypi.local"
# remove from know_hosts - ip
# host <hostname> returns either
# - <hostname>> has address <ip address>>
# - Host <hostname> not found: 3(NXDOMAIN)
# TODO: should check IP address is really an IP address
# TODO: dependency to host command
host_info=`host raspberrypi.local`
arr_host_info=($host_info)
echo ${arr_host_info[3]}
[[ ${arr_host_info[3]} =~ "found:" ]] && ret="INVALID" || ret="VALID"
echo host ip ${arr_host_info[3]} ${ret}
if [ "$ret" == "INVALID" ]
then
  exit 1
fi
ssh-keygen -R "${arr_host_info[3]}"


# add host in known hosts
ssh-keyscan -H "raspberrypi.local" >> $HOME/.ssh/known_hosts

echo "Launch script on remote pi"
pub_key_content=$(<$2)
echo "$pub_key_content"
ssh pi@raspberrypi.local "bash -s" < ./script_remote_config.sh "$1" "$3" \""$pub_key_content"\"




