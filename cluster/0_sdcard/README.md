# SD Card install

Valid: 02/02/2020

Complete doc is available [here](https://desertbot.io/blog/headless-raspberry-pi-4-ssh-wifi-setup)

For MacOSX, the procedure is as follow

## SD card preparation

In order to launch Raspbian on a Raspberry Pi in headless mode (no screen), a first step is to flash the OS on an SD 
card and configure it so that the RPi can boot and be accessed remotely over SSH.

*Step 1*: download and install [BalenaEtcher](https://www.balena.io/etcher/) (also available for Win/Linux)

*Step 2*: download Raspian [current version Buster](https://www.raspberrypi.org/downloads/raspbian/)

*Step 3*: flash on SD card

*Step 4*: remove/insert card and mount as disk (small FAT32 partition)

*Step 5*: to enable ssh, create `ssh` file at root of mounted boot partition (file can be empty and will be deleted 
automatically afterwards)

*Step 6*: by default, networking is enabled for ethernet. To enable Wifi, the proper configuration file should also be 
created on the boot partition.

Create `wpa_supplicant.conf` file at the root of the mounted boot partition and add the following content (change 
according to your configuration)

```
country=FR
ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
update_config=1

network={
    ssid="NETWORK-NAME"
    psk="NETWORK-PASSWORD"
}
```

More on [setting up wifi from the command line](https://www.raspberrypi.org/documentation/configuration/wireless/wireless-cli.md)

And a few links on flasshing RPi live 
- https://raspberrypi.stackexchange.com/questions/26578/is-it-possible-to-flash-the-pis-firmware-live
- https://raspberrypi.stackexchange.com/questions/37920/how-do-i-set-up-networking-wifi-static-ip-address

Issue with hostnames locally assigned with -2 or -3 suffixes over the network
https://serverfault.com/questions/652102/how-to-prevent-avahi-adding-2-to-hostnames

## Change hostname

Change raspberrypi with new hostname

    sudo nano /etc/hostname
    sudo nano /etc/hosts

Change password of pi user

create the `.ssh` folder (**IMPORTANT: restrict access**) and create the file `~/.ssh/authorized_keys` that contains the public key for the defined cluster.


