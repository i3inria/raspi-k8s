#!/bin/bash

echo "## on remote Raspberry ##"

# echo "new hostname is $1"
# echo "new pi password is $2"
# echo "content pub key is $3"

echo "Update the hostname in the two etc config files to $1"
# update nginx config for site and restart
# use sites-available to preserve symlink
sudo sed -i "s/raspberrypi/$1/g" /etc/hosts
sudo sed -i "s/raspberrypi/$1/g" /etc/hostname

echo "create protected .ssh folder"
mkdir .ssh
chmod go-rwx .ssh

echo "update user passwoord"
echo -e "raspberry\n$2\n$2" | passwd

touch .ssh/authorized_keys
echo "$3" > .ssh/authorized_keys

sudo reboot