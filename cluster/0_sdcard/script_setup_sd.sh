#!/bin/bash

#
# script to enable ssh and copy wifi config file on mounted sd card
#

if [ "$#" -ne 2 ]
then
  echo >&2 "usage: $0 sd_mount_point supplicant_file"
  echo >&2 "   - example: $0 /Volumes/boot wpa_supplicant_net1.conf"
  exit 1
fi

touch $1/ssh
cp $2 $1/wpa_supplicant.conf


