# Setting up a local Docker registry

In this document
- Deailed description of the registry installation
- How to secure the registry
- Ansible plabooks for automated install
- Using the REST API to query the registry
- Links and references


## Registry installation 


**On the master side (chosen to host the registry)**

set directories and config files
```
sudo mkdir /var/lib/registry 
sudo vi pi_registry_config.yml
sudo vi /etc/docker/daemon.json
docker run -d -p 5000:5000 --restart=always --name registry -v `pwd`/pi_registry_config.yml:/etc/docker/registry/config.yml registry:2
```
check that docker container starts properly, if error, delete and restart
```
docker logs registry
docker container rm registry
```
pull an image for DockerHub and push on the local repository
```
docker pull busybox
docker tag busybox pimaster.local:5000/busybox:default
docker push pimaster.local:5000/busybox:default
```

**On the client side**

Edit the Docker daemon config file and allow insecure access to the registry (set the IP address ot the reg’s host)
```
sudo vi /etc/docker/daemon.json

{
    "dns": ["192.168.0.254"],
    "insecure-registries": ["192.168.0.55:5000"]
}
```
then restart the service
```
sudo service docker restart
```
pulling the remote image
```
docker pull pimaster.local:5000/busybox
```
gives the following output
```
Using default tag: latest
Error response from daemon: Get https://pimaster.local:5000/v2/: http: server gave HTTP response to HTTPS client
```

## Secure registry setup

**On the master side**

```
cd docker/
mkdir -p certs
openssl req   -newkey rsa:4096 -nodes -sha256 -keyout certs/domain.key   -x509 -days 365 -out certs/domain.crt

docker container stop registry

docker run -d   --restart=always   --name registry   -v "$(pwd)"/certs:/certs   -e REGISTRY_HTTP_ADDR=0.0.0.0:443   -e REGISTRY_HTTP_TLS_CERTIFICATE=/certs/domain.crt   -e REGISTRY_HTTP_TLS_KEY=/certs/domain.key   -p 443:443   registry:2

docker logs registry

more certs/domain.crt 

docker pull busybox
docker tag busybox localhost:443/busybox:default
docker push localhost:443/busybox:default
```

**On the client side**

Install the certificate for the remote registry
```
sudo mkdir /etc/docker/certs.d
sudo mkdir /etc/docker/certs.d/pimaster.local:5000
sudo vi /etc/docker/certs.d/pimaster.local:5000/ca.crt
```
<&lt; COPY CRT FROM ABOVE MASTER >>

Not enough, need the system also to accept the root CA
```
sudo mkdir /usr/share/ca-certificates/local
sudo cp  /etc/docker/certs.d/pimaster.local:5000/ca.crt /usr/share/ca-certificates/local/pimaster.local.crt
```
add the new CA certificate as a trusted CA for the system (Raspberry way)
```
sudo dpkg-reconfigure ca-certificates
```
restart Docker daemon
```
service docker restart
```
test all is fine
```
openssl s_client -connect pimaster.local:443
sudo apt-get install lynx
lynx https://pimaster.local
```
try to pull image (use 443 here and not 5000)
```
docker pull pimaster.local:443/busybox:default
```

**One step key and csr generation:**
```
openssl req -new -newkey rsa:4096 -nodes \
    -keyout www.example.com.key -out www.example.com.csr \
    -subj "/C=US/ST=Denial/L=Springfield/O=Dis/CN=www.example.com"
```

**One step self signed passwordless certificate generation:**
```
openssl req -new -newkey rsa:4096 -days 365 -nodes -x509 \
    -subj "/C=US/ST=Denial/L=Springfield/O=Dis/CN=www.example.com" \
    -keyout www.example.com.key  -out www.example.com.crt
```

**Error messages**
*   `server gave HTTP response to HTTPS client`: means that by default, docker tries to access securely to remote registries, cannot default to http. Insecure is only for localhost.
*   `X509: certificate signed by unknown authority`: 
*   Means that on the client side, the CA is not trusted by the underlying OS. How to add this depends of the OS (Mac, Ubuntu, …)
*   On RapsberryPi: [https://raspberrypi.stackexchange.com/questions/76419/entrusted-certificates-installation/76421](https://raspberrypi.stackexchange.com/questions/76419/entrusted-certificates-installation/76421)
*   Create a folder in /usr/share/ca-certificates/
*   Add the ca.crt in this folder, then run sudo dpkg-reconfigure ca-certificates
*   `Error response from daemon: manifest for XXXX:XX not found`
*   Means that the image has not been pushed/pulled with the proper version identification (e.g., latest, default, …)


Additionaly [https://superuser.com/questions/226192/avoid-password-prompt-for-keys-and-prompts-for-dn-information](https://superuser.com/questions/226192/avoid-password-prompt-for-keys-and-prompts-for-dn-information)

## Ansible install

This folder/playbook is about setting up a Docker images registry inside the cluster so that nodes can share images 
more easily. Images can be stored on one node that has enough storage so that the other node can prune images 
without risks.


Main Docker page for the [registry image](https://hub.docker.com/_/registry
) and the page about its [configuration](https://docs.docker.com/registry/configuration/)


Some other links on private registry with self-signed certs
- Hackernoon's [Create a private local docker registry](https://hackernoon.com/create-a-private-local-docker-registry-5c79ce912620)
- Medium [similar post](https://medium.com/@ifeanyiigili/how-to-setup-a-private-docker-registry-with-a-self-sign-certificate-43a7407a1613)



On master

    docker run -d  --restart=always   --name registry   -v /home/pi/docker/certs:/certs           -p 5443:443           -e REGISTRY_HTTP_ADDR=0.0.0.0:443           -e REGISTRY_HTTP_TLS_CERTIFICATE=/certs/domain2.crt           -e REGISTRY_HTTP_TLS_KEY=/certs/domain2.key           registry:2
    docker logs registry


Then on one node
    
    docker pull busybox
    docker tag busybox pimaster.local:5443/busybox:default2
    docker push pimaster.local:5443/busybox:default2
  
Then on yet another node

    docker pull pimaster.local:5443/busybox:default2

## Registry REST API

Can query the REST APi of the registry

For that, need to provide the registry CA cert to accept the connection.

    wget --ca-certificate=//etc/docker/certs.d/pimaster.local\:5443/ca.crt  -v https://pimaster.local:5443/v2/_catalog

    curl  --cacert  /etc/docker/certs.d/pimaster.local\:5443/ca.crt  https://pimaster.local:5443/v2/_catalog

return

    {"repositories":["busybox"]}

To get the list of images, then to get the tags for a specific image

    wget --ca-certificate=//etc/docker/certs.d/pimaster.local\:5443/ca.crt  -v https://pimaster.local:5443/v2/busybox/tags/list
    
    curl  --cacert  /etc/docker/certs.d/pimaster.local\:5443/ca.crt  https://pimaster.local:5443/v2/busybox/tags/list
    
return

    {"name":"busybox","tags":["default"]}


## Links and references

Some links from the Docker documentation
*   [https://docs.docker.com/registry/](https://docs.docker.com/registry/)
*   [https://docs.docker.com/registry/deploying/](https://docs.docker.com/registry/deploying/)
*   [https://docs.docker.com/registry/insecure/](https://docs.docker.com/registry/insecure/)
*   [https://docs.docker.com/registry/configuration/](https://docs.docker.com/registry/configuration/)

Related docker doc
*   [https://docs.docker.com/engine/security/https/](https://docs.docker.com/engine/security/https/)

Some other tutorials
*   [https://blog.eleven-labs.com/fr/mise-en-place-docker-registry-privee/](https://blog.eleven-labs.com/fr/mise-en-place-docker-registry-privee/)
*   [https://mesosphere.github.io/marathon/docs/native-docker-private-registry.html](https://mesosphere.github.io/marathon/docs/native-docker-private-registry.html)
*   [https://stackoverflow.com/questions/33054369/how-to-change-the-default-docker-registry-from-docker-io-to-my-private-registry](https://stackoverflow.com/questions/33054369/how-to-change-the-default-docker-registry-from-docker-io-to-my-private-registry)
*   [https://blog.docker.com/2013/07/how-to-use-your-own-registry/](https://blog.docker.com/2013/07/how-to-use-your-own-registry/)
*   [https://github.com/geerlingguy/raspberry-pi-dramble/issues/108](https://github.com/geerlingguy/raspberry-pi-dramble/issues/108)

Default registry conf: 
* [https://github.com/docker/distribution/blob/master/cmd/registry/config-example.yml](https://github.com/docker/distribution/blob/master/cmd/registry/config-example.yml)

Adding certificates for trusted registry

