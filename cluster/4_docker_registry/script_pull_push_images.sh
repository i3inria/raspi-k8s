#!/bin/bash

#
# Shell script to test pull/push of an image into a local docker registry
#

## Ubuntu image

# Pull the default ubuntu image
docker pull ubuntu:16.04

# tag and push it locally
docker tag ubuntu:16.04 localhost:5432/ubuntu:16.04
docker push localhost:5432/ubuntu:16.04


