#!/bin/bash

# add the master's CA cert
echo "$1" >> /usr/local/share/ca-certificates/server-ca.crt
update-ca-certificates

# install k3s on client node
curl -sfL https://get.k3s.io | K3S_URL="https://$2:6443" K3S_TOKEN="$3"  sh -
