
#

### Existing projects and playbooks

*   [https://github.com/Project31/ansible-kubernetes-openshift-pi3](https://github.com/Project31/ansible-kubernetes-openshift-pi3)
*   [https://github.com/rak8s/rak8s](https://github.com/rak8s/rak8s)
*   [https://github.com/mrlesmithjr/ansible-rpi-k8s-cluster](https://github.com/mrlesmithjr/ansible-rpi-k8s-cluster)
*   [https://jls42.org/raspberry-pi/installation-de-kubernetes-sur-raspberry-pi-via-ansible/](https://jls42.org/raspberry-pi/installation-de-kubernetes-sur-raspberry-pi-via-ansible/)
*   [https://jls42.org/raspberry-pi/initialisation-auto-de-raspbian-sur-raspberry-pi/](https://jls42.org/raspberry-pi/initialisation-auto-de-raspbian-sur-raspberry-pi/)

## Install Cluster A

Initial setup
*   Created pimaster1 and pinode2/3/4 cluster with default raspbian
*   Enabled ssh on all hosts

generate cluster_a_rsa pub/priv keys
```
ssh-keygen -t rsa
```
copy pub key on remote node
```
scp cluster_a_rsa.pub pi@pimaster1.local:cluster_a_rsa.pub
```
login and copy the key in authorized_keys file
```
ssh pi@pimaster1
mkdir .ssh; touch .ssh/authorized_keys; cat ~/cluster_a_rsa.pub >> .ssh/authorized_keys; rm cluster_a_rsa.pub; chmod go-rx .ssh
```
launch script on all nodes (without ` --ask-sudo-pass`, will run as user)

`ansible-playbook hosts_test_script.yml --inventory-file=hosts_inventory_cluster_a.yml --ask-sudo-pass` 


## RPI Issues

Insert SDCart (MacOSX)
```
diskutil list
```
-> assume sdcard on /dev/disk2

Use SD preparation file from [https://jls42.org/raspberry-pi/initialisation-auto-de-raspbian-sur-raspberry-pi/](https://jls42.org/raspberry-pi/initialisation-auto-de-raspbian-sur-raspberry-pi/)

**But for MacOSX**

need to umount the sdcard for DD
[https://unix.stackexchange.com/questions/271471/running-dd-why-resource-is-busy](https://unix.stackexchange.com/questions/271471/running-dd-why-resource-is-busy) 
```
sudo diskutil unmountDisk /dev/disk2
```
update dd parameters in file
```
...sudo dd of=${msd_fs} bs=4M conv=fsync ..
```
to
```
...sudo dd of=${msd_fs} bs=4m conv=sync ..
```


Reboot during install
*   For example, when adding cgroup info in /boot/cmdline.txt
*   [https://www.jeffgeerling.com/blog/2018/reboot-and-wait-reboot-complete-ansible-playbook](https://www.jeffgeerling.com/blog/2018/reboot-and-wait-reboot-complete-ansible-playbook)
*   Starting Ansible 2.7, reboot command. 
*   Use together with using async and the special[ wait_for_connection module](http://docs.ansible.com/ansible/latest/modules/wait_for_connection_module.html) to wait for ssh connection to be up and running again. Add proper delay.
*   Need to add a delay to let Ansible shutdown properly ()
*   Use shell instead of command so that can chain operations with &&
*   Check if file needs to be changed, return task_result. This allow to bypass reboot second time the script is launched.

Ansible and ssh
*   Inventory files with ssh parameters: 
*   [https://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html](https://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html)
*   [https://serverfault.com/questions/628989/how-to-set-default-ansible-username-password-for-ssh-connection](https://serverfault.com/questions/628989/how-to-set-default-ansible-username-password-for-ssh-connection)
*   [https://stackoverflow.com/questions/21160776/how-to-execute-a-shell-script-on-a-remote-server-using-ansible](https://stackoverflow.com/questions/21160776/how-to-execute-a-shell-script-on-a-remote-server-using-ansible)
*   [https://linuxfr.org/users/skhaen/journaux/deploiement-et-automatisation-avec-ansible-partie-1](https://linuxfr.org/users/skhaen/journaux/deploiement-et-automatisation-avec-ansible-partie-1)
*   [https://stackoverflow.com/questions/44734179/specifying-ssh-key-in-ansible-playbook-file](https://stackoverflow.com/questions/44734179/specifying-ssh-key-in-ansible-playbook-file)

Where to put private keys, check vault also

[https://stackoverflow.com/questions/33667941/ansible-where-private-key-should-be-placed](https://stackoverflow.com/questions/33667941/ansible-where-private-key-should-be-placed)

[https://shadow-soft.com/turbo-charge-your-ansible/](https://shadow-soft.com/turbo-charge-your-ansible/)


# 


# RPi Clusters


## Cluster A

**Dashboard token**


```
kubectl get secret -n kube-system | grep k8sadmin | cut -d " " -f1 | xargs -n 1 | xargs kubectl get secret  -o 'jsonpath={.data.token}' -n kube-system | base64 --decode
```

eyJhbGciOiJSUzI1NiIsImtpZCI6IiJ9.eyJpc3MiOiJrdWJlcm5ldGVzL3NlcnZpY2VhY2NvdW50Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9uYW1lc3BhY2UiOiJrdWJlLXN5c3RlbSIsImt1YmVybmV0ZXMuaW8vc2VydmljZWFjY291bnQvc2VjcmV0Lm5hbWUiOiJrOHNhZG1pbi10b2tlbi16bG1oNCIsImt1YmVybmV0ZXMuaW8vc2VydmljZWFjY291bnQvc2VydmljZS1hY2NvdW50Lm5hbWUiOiJrOHNhZG1pbiIsImt1YmVybmV0ZXMuaW8vc2VydmljZWFjY291bnQvc2VydmljZS1hY2NvdW50LnVpZCI6Ijk0MDhiNjA5LTZmN2YtMTFlOS1hMmJmLWI4MjdlYjEyNWE2MiIsInN1YiI6InN5c3RlbTpzZXJ2aWNlYWNjb3VudDprdWJlLXN5c3RlbTprOHNhZG1pbiJ9.xiu1pI7xU0Bht3jogxRaxozil-h424A_q0DKgcTpV7h-Cqcxqs_AVMBeG35eV9q3tdGd365ILN-uDklNqFQp6wCH7eRD8ckvx2K2mmtZEnY0rj63fXdNZ7wBemmbBxOqSAoDrm-0YWEVpKHRBp8y9pr4Cy4asYhq7StAJnKz4XiXNU5kMOjfl7rra59XgZpXR69Wj5W2TxXGmRystzoSXZ9R0_fWKvIItf_otAwfNdD6SYdYExE-M0Ek9OOYMkzWVqzHeCxON3ZYBeBldCn2Kmt0HSGwK0qS2xvnUn-WMMr8_llQCJaM6sx2xtq5jLql4UZA2ThWYwzTUBGmOq_KrQ


## Cluster B

Insalling containters

But first, how to do port forwarding to access applications ? Exemple with Redis

[https://kubernetes.io/docs/tasks/access-application-cluster/port-forward-access-application-cluster/](https://kubernetes.io/docs/tasks/access-application-cluster/port-forward-access-application-cluster/)

*   Run kube-proxy

Create a pod yaml for redis (pod_redis.yaml). Select a `nodePort` that will be the public k8s port for this service, here 31215.
```
apiVersion: v1
kind: Service
metadata:
  name: redis
  labels:
	app: redis
spec:
  type: NodePort
  ports:
	- port: 6379
  	protocol: TCP
  	targetPort: 6379
  	nodePort: 31215
  selector:
	app: redis
---
apiVersion: apps/v1beta1 # for versions before 1.6.0 use extensions/v1beta1
kind: Deployment
metadata:
  name: redis
spec:
  replicas: 1
  template:
	metadata:
  	labels:
    	app: redis
	spec:
  	containers:
  	- name: redis
    	image: arm32v7/redis
    	imagePullPolicy: Always
    	ports:
    	- containerPort: 6379
      	protocol: TCP
```
install the pod
```
kubectl apply -f pod_redis.yml
```
get service description with port being exposed
```
kubectl get svc | grep redis
```
This returns
```
redis        NodePort	10.99.254.209   <none>    	6379:31215/TCP   2d
```
the redis official port (6379) on the node is redirected to the 31215 public port of K8S (port range is: )

on the master (or any node), access redis-cli on the public port. If redis-cli is installed locally
```
redis-cli -p 31215
```
forward the port for the service
```
kubectl port-forward redis-master-765d459796-258hz 6379:6379
```
use redis client

    redis-cli

heck on which node the pod is deployed (here for node red)

forward the port

use kube proxy

Plenty of deployments
*   [https://github.com/hyperbolic2346/kubernetes](https://github.com/hyperbolic2346/kubernetes)
*   [https://github.com/kairen?language=python&tab=repositories](https://github.com/kairen?language=python&tab=repositories)

Markdown renderer
*   [https://medium.com/@oliver_hu/docker-kubernetes-on-raspberry-pi-3-cb787def29d5](https://medium.com/@oliver_hu/docker-kubernetes-on-raspberry-pi-3-cb787def29d5)
*   [https://hub.docker.com/r/functions/markdownrender/](https://hub.docker.com/r/functions/markdownrender/)

## Misc.

**Shell script trick**

on the fist line of the shell_script.sh file, add `-x` will display verbose output on the console


```
#!/bin/bash -x
```


**RGB cpu meter** : [https://gist.github.com/AnthonyDiGirolamo/9491567](https://gist.github.com/AnthonyDiGirolamo/9491567)

**Raspberry GPIO**

*   [http://silanus.fr/bts/activites/raspberry/gpioRaspberryPi-SN2V1.html](http://silanus.fr/bts/activites/raspberry/gpioRaspberryPi-SN2V1.html)
*   [http://codefoster.com/pi-basicgpio/](http://codefoster.com/pi-basicgpio/)



### Benchmarking
SD card benchmarks & SD card overclock
* [https://www.pidramble.com/wiki/benchmarks/microsd-cards](https://www.pidramble.com/wiki/benchmarks/microsd-cards)

Overclocking and potential issues
* [https://www.jeffgeerling.com/blog/2016/how-overclock-microsd-card-reader-on-raspberry-pi-3](https://www.jeffgeerling.com/blog/2016/how-overclock-microsd-card-reader-on-raspberry-pi-3)


### Technologies

Below is a list of the tools used as part of the deployment on RPi


**Kubernetes ()**

**Etcd ([https://coreos.com/etcd](https://coreos.com/etcd))**

highly-available distributed KV (key-value) storage used by K8S services.

Manual install guide vs Kubernetes POD vs Docker container

[https://icicimov.github.io/blog/kubernetes/Kubernetes-cluster-step-by-step-Part3/](https://icicimov.github.io/blog/kubernetes/Kubernetes-cluster-step-by-step-Part3/)

**CouchDB ()**

**CNI - Flannel ([https://github.com/coreos/flannel](https://github.com/coreos/flannel)) Weave ([https://www.weave.works/blog/weave-net-kubernetes-integration/](https://www.weave.works/blog/weave-net-kubernetes-integration/)) or Calico, Kube-router**


*   CNI allow all pods and nodes to communicate. They automate/facilitate the creation of routes, filtering and network policies between the hosts of the cluster.
*   Good presentation in French : [https://www.objectif-libre.com/fr/blog/2018/07/05/comparatif-solutions-reseaux-kubernetes/](https://www.objectif-libre.com/fr/blog/2018/07/05/comparatif-solutions-reseaux-kubernetes/) 
*   Flannel is a simple and easy way to configure a layer 3 network fabric designed for Kubernetes. flannel is also widely used outside of kubernetes. When deployed outside of kubernetes, etcd is always used as the datastore.

**OpenWhisk ()**

[https://openwhisk.apache.org/documentation.html#operators](https://openwhisk.apache.org/documentation.html#operators)

**OpenFaas ()**

**Kafka ()**

**Consul ()**

**Ansible ([https://docs.ansible.com/](https://docs.ansible.com/))**

Configuration management system. Store Playbooks (recipes) that can be applied to servers. Login thru ssh, Playbook in yaml, can be stored in git. Similar to Chef, Puppet(?). Jinja templates in Ansible allows loops and conditionals, text manipulation and temporary variables.

[https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html)

[https://kublr.com/blog/approaches-to-configuration-management-chef-ansible-and-kubernetes/](https://kublr.com/blog/approaches-to-configuration-management-chef-ansible-and-kubernetes/)

Install on MacOSX [https://hvops.com/articles/ansible-mac-osx/](https://hvops.com/articles/ansible-mac-osx/)

**Rancher ()**

**Kind ()**

**BlueMix ()**

**Docker, LinuxKit, Hyper-V and Linux containers on Windows (LCOW)**

**Envoy Service mesh**
*   [https://hackernoon.com/service-mesh-with-envoy-101-e6b2131ee30b](https://hackernoon.com/service-mesh-with-envoy-101-e6b2131ee30b)
*   [https://hackernoon.com/microservices-monitoring-with-envoy-service-mesh-prometheus-grafana-a1c26a8595fc](https://hackernoon.com/microservices-monitoring-with-envoy-service-mesh-prometheus-grafana-a1c26a8595fc)

# Terminology

**RBAC** : role-based access control (by opposition to identity-based access control)

**CNI** : Content Network Interface

**LCOW** : Linux containers on Windows

