# Docker


## Basic commands

Cheatsheets

*   Docker: https://developers.redhat.com/promotions/docker-cheatsheet/

Check DNS for Docker
```
docker run busybox ping -c 10 www.cnn.com
```
## Docker on Mac

After Docker install, start with
```
bash --login '/Applications/Docker/Docker Quickstart Terminal.app/Contents/Resources/Scripts/start.sh'
```
test list
```
docket list
```
create Dockerfile test dir
```
cd /WORK/DVLP/
mkdir DOCKER; cd DOCKER/
mkdir dockerfile; cd dockerfile/
```
edit basic Dockerfile
```
vi Dockerfile
```
and add
```
FROM alpine
CMD ["echo","Hello world!"]
```
build image from Dockerfile and run
```
docker build .
docker run --name test 643311d9f267
docker rm test

docker run hello-world
```

# Bash (for Dockerfiles)

Post from bash

[https://stackoverflow.com/questions/53943485/sending-post-request-from-bash-script](https://stackoverflow.com/questions/53943485/sending-post-request-from-bash-script)

View headers

[https://stackoverflow.com/questions/11836238/using-curl-to-download-file-and-view-headers-and-status-code](https://stackoverflow.com/questions/11836238/using-curl-to-download-file-and-view-headers-and-status-code)

Shell Parameter Expansion

[https://stackoverflow.com/questions/2013547/assigning-default-values-to-shell-variables-with-a-single-command-in-bash/25895084](https://stackoverflow.com/questions/2013547/assigning-default-values-to-shell-variables-with-a-single-command-in-bash/25895084)

String not defined

[https://stackoverflow.com/questions/228544/how-to-tell-if-a-string-is-not-defined-in-a-bash-shell-script](https://stackoverflow.com/questions/228544/how-to-tell-if-a-string-is-not-defined-in-a-bash-shell-script)

Check if env var exists

[https://stackoverflow.com/questions/39296472/how-to-check-if-an-environment-variable-exists-and-get-its-value](https://stackoverflow.com/questions/39296472/how-to-check-if-an-environment-variable-exists-and-get-its-value)

Iterate array of vars

[https://stackoverflow.com/questions/27702452/loop-through-a-comma-separated-shell-variable](https://stackoverflow.com/questions/27702452/loop-through-a-comma-separated-shell-variable)

env/arg variables in Dockerfiles

[https://vsupalov.com/docker-arg-env-variable-guide/#setting-env-values](https://vsupalov.com/docker-arg-env-variable-guide/#setting-env-values)


