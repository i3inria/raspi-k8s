#!/bin/bash

##
## Reset and uninstall k8s on the node
##

#
kubeadm reset

#
sudo apt-get purge -y kubeadm kubectl kubelet kubernetes-cni kube*
sudo apt-get -y autoremove
sudo rm -rf ~/.kube

# need to reboot
