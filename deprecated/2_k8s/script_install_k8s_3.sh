#!/bin/bash

## Completes the configuration of the master nodes by copying the admin conf on the current user
##
##

# setup kube config
#
mkdir -p $HOME/.kube
rm $HOME/.kube/config
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config

# need to reboot
