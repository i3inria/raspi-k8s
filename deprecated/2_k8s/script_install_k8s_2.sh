#!/bin/bash

## Initialize k8s on the master.
## - use Flannel (by setting cidr) but pod installed later
## - ignore as latest docker version officialy not supported (ignore-preflight-errors)
##
## Token/hash for joining displayed on the console. Should be returned/captured and parsed for other nodes to join
##
## Parameters:
##      $1 : master IP address

# only do kubeadm init and parser result to extract the token/hash/ip
#
kubeadm init --token-ttl=0 --pod-network-cidr=10.244.0.0/16 --apiserver-advertise-address=$1 --ignore-preflight-errors=SystemVerification,Mem

