#!/bin/bash

##
## Basic K8s package installs for both master and nodes
##
## For using Dashboard on Raspberry Pi, v 1.10.8-00 is currently the most recent version possible (v1.13 currently latest)
## Assumes a recent version of Docker is installed.

sudo apt-get update -o Acquire::ForceIPv4=true

# Update keys and sourcelists for kubernetes
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add - && \
  echo "deb http://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list && \
  sudo apt-get update -q

# Need to install cni before
#
sudo apt-get install kubernetes-cni=0.6.0-00

# install kubeXXX=1.10.8-00
#
sudo apt-get install -qy kubelet=1.10.8-00 kubectl=1.10.8-00 kubeadm=1.10.8-00

sudo apt-get install -qy kubernetes-cni
sudo apt-get install -qy kubelet kubectl kubeadm

