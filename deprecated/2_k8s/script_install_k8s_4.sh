#!/bin/bash

## Install the network overlay pod Flannel on the master
##
##

sleep 10
kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml

sudo sysctl net.bridge.bridge-nf-call-iptables=1
