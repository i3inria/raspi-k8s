## OpenWhisk on MacOSX

Prerequisites: install Docker

Youtube tutorials

[https://www.youtube.com/watch?v=RHzQf5eoAwc](https://www.youtube.com/watch?v=RHzQf5eoAwc)

Install OpenWhisk with docker-compose

Visit
[https://github.com/apache/incubator-openwhisk#native-development](https://github.com/apache/incubator-openwhisk#native-development)  

then
[https://github.com/apache/incubator-openwhisk-devtools/blob/master/docker-compose/README.md](https://github.com/apache/incubator-openwhisk-devtools/blob/master/docker-compose/README.md)

Do

```
git clone https://github.com/apache/incubator-openwhisk-devtools.git
cd incubator-openwhisk-devtools/docker-compose
make quick-start
```


[warning - had to do multiple times before is “completed”]

At then end, get the error message 
```
There was an internal server error.
ok: whisk auth set. Run 'wsk property get --auth' to see the new value.
ok: whisk API host set to https://192.168.99.100
error: Authenticated user does not have namespace 'guest'; set command failed: The connection failed, or timed out. (HTTP status code 500)
make: *** [init-whisk-cli] Error 1
```

Install OpenWhisk CLI - (wsk)

Visiti [https://github.com/apache/incubator-openwhisk-cli](https://github.com/apache/incubator-openwhisk-cli) 
*   Download binary for target platform (done)
*   Or source and compile (not done yet - need Go)
*   Or install with brew: `brew install wsk`

Uncompress and launch
```
../OpenWhisk_CLI-0.9.0-incubating-mac-386/wsk  -i --apihost http://192.168.99.100:8888 namespace list
```

Should display
```
namespaces
Guest
```

Use CLI without Bluemix account ?

[https://stackoverflow.com/questions/44463957/can-we-use-openwhisk-cli-without-ibm-bluemix-account?noredirect=1&lq=1](https://stackoverflow.com/questions/44463957/can-we-use-openwhisk-cli-without-ibm-bluemix-account?noredirect=1&lq=1)

OpenWhisk on Kubernetes cluster from Bluemix container

[https://github.com/IBM/OpenWhisk-on-Kubernetes](https://github.com/IBM/OpenWhisk-on-Kubernetes)

OpenWhisk catalog

[https://github.com/apache/incubator-openwhisk-catalog](https://github.com/apache/incubator-openwhisk-catalog)


# 

