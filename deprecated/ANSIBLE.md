# Ansible

Ansible is an open-source software provisioning, configuration management, and application-deployment tool.

Check the [5 minutes presentation](https://sed-paris.gitlabpages.inria.fr/developer-meetups/2019-05-28/ansible_devmeetup.pdf
) in #10 of the [Developer Meetups]([https://sed-paris.gitlabpages.inria.fr/developer-meetups/](https://sed-paris.gitlabpages.inria.fr/developer-meetups/)).

## Install Ansible

https://docs.ansible.com/ansible/latest/index.html

## Tips and tricks, cheatsheets

*   [http://codeheaven.io/15-things-you-should-know-about-ansible/](http://codeheaven.io/15-things-you-should-know-about-ansible/)
*   [https://ansible-tips-and-tricks.readthedocs.io/en/latest/ansible/commands/](https://ansible-tips-and-tricks.readthedocs.io/en/latest/ansible/commands/)

## Ansible install on Raspberry Pi

[https://raspberrypi.stackexchange.com/questions/37920/how-do-i-set-up-networking-wifi-static-ip-address](https://raspberrypi.stackexchange.com/questions/37920/how-do-i-set-up-networking-wifi-static-ip-address)
