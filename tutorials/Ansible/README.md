# Details of test ansible scripts

[Latest Ansible documentation](https://docs.ansible.com/ansible/latest/index.html) with installation procedure.

## Testbed setup

| Inria setup | Home setup |
| --- | --- |
| ![Inria setup](documentation/assets/cluster_a_inria.png) | ![Home setup](documentation/assets/cluster_a_home.png)|

## Inventory file

The inv_test_cluster_a.yml inventory file describes a cluster with 4 nodes
based on hostnames.

Two groups are defined, one (All) with all the nodes and one (master) with 
only the master node. Each group is independently defined. An better alternative 
would be to define All as a combination of a Master group and a Node group.

Each host has 2 host specific variables, an array of strngs and a string.

Additionally, the All group defiens a number of variables to be used in the 
various scripts. The master group defines a few variables.

## script

The  script_test.sh script file is used in relevant Playbooks to demonstrate 
the use of environment variables and parameters set by the playbooks.

## Playbooks

A number of playbooks are provided to illustrate various issues. In most cases,
a playbook will combine several issues/solutions together.

For each playbook, a short explanation is provided, along with the command line
to start it.

+ **pb_test_loop.yml**: loop over a list of 2 items on all nodes of the inventory and add a line in a log file with the loop item name and a var of the current node.

+ **pb_test_loop2.yml**: extends the previous PB. Instead of defining the list of items to loop over in the PB, use either a list for the group or for the host.

+ **pb_test_loop_file_creation.yml**: waits for a file to be created before continuing.

+ **pb_test_multi_groups.yml**: runs commands on different sets of hosts using filters, groups.

+ **pb_test_nmcli.yml**: sets static IP addresses using nmcli module, use markers in config files.

+ **pb_test_output.yml**: how to use console output from command.

+ **pb_test_output_parsing**: extract data from a shell command output using regexp and save the results on a file on the host.

+ **pb_test_reboot**: how to reboot, wait for hosts to be online and continue the script.

+ **pb_test_script**: upload and run a shell script as root and display its console output.
