#!/bin/bash

ls
echo $1
echo $HOSTNAME
echo 'vault-option name="KEYSTORE_PASSWORD" value="MASK-5dOaAVafCSd"'

echo 'Your Kubernetes master has initialized successfully!

To start using your cluster, you need to run the following as a regular user:

  mkdir -p $HOME/.kube
  sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
  sudo chown $(id -u):$(id -g) $HOME/.kube/config

You should now deploy a pod network to the cluster.
Run "kubectl apply -f [podnetwork].yaml" with one of the options listed at:
  https://kubernetes.io/docs/concepts/cluster-administration/addons/

You can now join any number of machines by running the following on each node
as root:

  kubeadm join 192.168.2.50:6443 --token mu8std.b49d5yrx0xcndt2v --discovery-token-ca-cert-hash sha256:e940168aeb1651d64e5235f0326b6ab3b2b02545ebbf92119649d0d529cdebdc'
